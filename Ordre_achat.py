# AMAConsulting/Ordre_achat.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from .base import Base
from .Soumission import Soumission

class Ordre_achat(Base):
    __tablename__ = 'ordre_achat'
    id = Column(Integer, primary_key=True)
    date_oa = Column(String)
    date_debut = Column(String)
    date_fin = Column(String)
    soumission_id = Column(Integer, ForeignKey('soumission.id'))

    def __repr__(self):
        return "<Ordre_achat( oa_id='%s', date_oa='%s', date_debut='%s', date_fin='%s', soumission_id='%d')>" \
        % (self.id, self.date_oa, self.date_debut, self.date_fin, self.soumission_id)

    def afficher(session):
        resultat = [instance.__dict__ for instance in session.query(Ordre_achat)]
        return resultat
    
    def afficher_id(oa_id, session):
        for instance in session.query(Ordre_achat).filter(Ordre_achat.id == oa_id):
            return instance

    def lister(session):
        return [instance for instance in session.query(Ordre_achat)]    

    def afficher_oa_consultant(email, session):
        for instance in session.query(Ordre_achat).filter(
            Ordre_achat.soumission_id.in_(session.query(Soumission.id).filter(Soumission.consultant == email))):
            return instance

    def afficher_oa_client(client, session):
        for instance in session.query(Ordre_achat).filter(
                Ordre_achat.soumission_id.in_(session.query(
                    Soumission.id).filter(Soumission.client == client))):
            return instance
        
    def afficher_oa_recruteur(recruteur, session):
        for instance in session.query(Ordre_achat).filter(
                Ordre_achat.soumission_id.in_(session.query(
                    Soumission.id).filter(Soumission.recruteur == recruteur))):
            return instance

    def lister_oa_id_par_recruteur(email, session):
        oa_id = [instance for instance in session.query(Ordre_achat.id).filter(
            Ordre_achat.soumission_id.in_(session.query(Soumission.id).filter(Soumission.recruteur == email)))]
        return oa_id
    
    def afficher_id_par_soumission_id(soumission_id, session):
        list_id = [instance for instance in session.query(Ordre_achat.id).filter(
            Ordre_achat.soumission_id == soumission_id)]
        return list_id

    def ajouter_multi(ordre_achats, session):
        for ordre_achat in ordre_achats:
            session.add(ordre_achat)

    def ajouter(ordre_achat, session):
        session.add(ordre_achat)
        session.commit()

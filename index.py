# AMAConsulting/index.py


# ------ IMPORTS -----

from flask import Flask
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import session
from flask import url_for
from functools import wraps
from .database import Database
from base64 import b64encode
import hashlib
import uuid
import calendar
from datetime import datetime, timedelta, date
from dateutil.relativedelta import *
import math
# import io
import os
# import configparser
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select
from .base import *
from .Abonnement import *
from .Avenant import *
from .Client import *
from .Consultant import *
from .Contact import *
from .Demande import *
from .Dotation import *
from .Offre import *
from .Ordre_achat import *
from .Recruteur import *
from .Soumission import *
from .Tenant import *
from .FeuilleDeTemps import *
from .Notification import *
from .Facture import *
from .Transaction import *


app = Flask(__name__)
app.config.from_object(__name__)

app.config.from_envvar('FLASK_SETTINGS', silent=True)
error_1 = "Identifiant ou mot de passe incorect ! Veuillez réessayer s'il vous plait !"
error_2 = "Aucun utilisateur avec cet identifiant dans le système !"
error_3 = "Tous les champs sont obligatoires !"
error_4 = ("Un utilisateur avec ce courriel existe déja !\n Veuillez vérifier "
           + "vos informations !")
info_1 = "Vous êtes à présent déconnecté !"

list_month_fr = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
                 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre',
                 'Décembre']

days_week = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi",
             "Dimanche"]

# ----- CONFIGURATION -----

engine = create_engine('sqlite:///db/ama.db?check_same_thread=False')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
db_session = DBSession()

# @Description   : Permet de lire le fichier de configuration du projet
# Load default config and override config from an environment variable
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'db/db.db'),
    SECRET_KEY='coopama2019',
    SECRET_PASSWORD_SALT='coopama2019',
    USERNAME='coop.ama.consulting@gmail.com',
    PASSWORD='coopama2019'
))


# @Description   :Permet d'accéder à la base de données du projet
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        g._database = Database()
    return g._database

# @Description   : Ferme l'accés à la base de données lorsque l'application
#                | est fermée
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.disconnect()


# ----- WRAPS / ERROR HANDLER-----

# @Situation   : Permet de définir un accés restreint aux utlisateurs
#              | authentifiés sur l'application
def authentication_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not is_authenticated(session):
            return send_unauthorized()
        return f(*args, **kwargs)
    return decorated

# @Situation   : Erreur 404 O^_^O
# @Credit      : http://flask.pocoo.org/docs/0.12/patterns/errorpages/
@app.errorhandler(404)
def page_not_found(e):

    return render_template('404.html'), 404

# ----- ROUTES -----

# @Situation   : Page d'accueil de l'application
@app.route('/', methods=['GET', 'POST'])
def home():

    if "id" in session:
        return redirect("/dashboard")

    else:
        return render_template('index.html')

# @Situation   : Inscription nouveau membre
@app.route('/new_member', methods=['GET', 'POST'])
def new_member():
    return render_template('inscription_user.html')

# @Situation   : Connection de l'utilisateur
# @Return      : Création de la clé de session 'id'
@app.route('/login', methods=['GET', 'POST'])
def login():

    username = request.form["email"]
    password = request.form["password"]

    if username == "" or password == "":
        return render_template("index.html", error=error_1)
    else:
        user = get_db().get_user_login_info(username)
        if user is None:
            return render_template("index.html", error=error_2)

        salt = user[0]

        hashed_password = (hashlib.sha512(str(password + salt).encode("utf-8"))
                           .hexdigest())

        if hashed_password == user[1]:
            id_session = uuid.uuid4().hex
            get_db().save_session(id_session, username)
            session["id"] = id_session

            return redirect("/dashboard")

        else:
            return render_template("index.html", error=error_1)


# @Situation   : Inscription d'un nouveau membre
# @Return      : Inscription et envoi d'un email de confirmation
@app.route('/new_member_submit', methods=['GET', 'POST'])
def new_member_submit():
    nom = request.form['nom']
    prenom = request.form['prenom']
    tel = request.form['telephone']
    email = request.form['email']
    password = request.form['password']
    entreprise = request.form['entreprise']
    forfait = request.form['forfait']

    if (nom == "" or prenom == "" or tel == "" or email == ""
       or password == "" or entreprise == ""):

        return render_template("inscription_user.html", login=False,
                               errors=error_3)

    if not get_db().get_user_exists(email):
        salt = uuid.uuid4().hex
        hashed_password = (hashlib.sha512(str(password + salt)
                                          .encode("utf-8")).hexdigest())

        get_db().insert_new_member(nom, prenom, email, tel, entreprise,
                                   salt, hashed_password)

        user_infos = [nom, prenom, email, password, entreprise]
        html = render_template('activate.html', user_infos=user_infos)
        subject = "Identifiants de connexion"
        send_email('coop.ama.consulting@gmail.com', email, subject, html,
                   None)
        
        tenant = Tenant(entreprise=entreprise, email=email, telephone=tel)
        Tenant.ajouter(tenant, db_session)
        
        abo_date_debut = (datetime.now()).strftime("%Y-%m-%d")
        abo_date_fin = (datetime.now() + timedelta(days=365)).strftime("%Y-%m-%d")
        abonnement = Abonnement(entreprise=entreprise, date_debut=abo_date_debut, date_fin=abo_date_fin,
                                offre=forfait, etat="en_cours")
        
        Abonnement.ajouter(abonnement, db_session)
        
        return redirect('confirmation/inscriptionSuccess')
    else:
        return redirect('confirmation/inscriptionFailed')


# @Situation   : Mot de passe oublié
# @Return      : Page pour entrer son courriel
@app.route("/reset_password", methods=['GET', 'POST'])
def password_reset():
    return render_template("reset_password.html")


@app.route("/reset_password_change", methods=['GET', 'POST'])
def password_reset_change():
    username = request.form["email"]
    password = request.form["password"]
    salt = uuid.uuid4().hex
    hashed_password = (hashlib.sha512(str(password + salt)
                                      .encode("utf-8")).hexdigest())

    infos_user = get_db().get_user_all_infos(username)
    get_db().reset_password(username, salt, hashed_password, infos_user[1])

    return render_template("confirmation.html",
                           type_conf="reset_password_confirm")


# @Situation   : Inscription nouveau membre
@app.route('/dashboard', methods=['GET', 'POST'])
def dashboard():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        notifications = Notification.afficher_toute_notifs_non_lu(username, db_session)

        if notifications is not None:
            notifications.sort(key=lambda n: n.date, reverse=True)

        today_weekday = datetime.today().weekday() + 1
        today_date = get_date()
        today_mois = get_mois()

        if utype == 1:
            details = get_details_dashboard(username)
            subscription = get_subscription(username)

            return render_template('dashboardRecruteur.html',
                                   user_infos=user_infos,
                                   utype=utype, notifications=notifications,
                                   today_date=today_date,
                                   today_weekday=today_weekday,
                                   subscription=subscription, details=details,
                                   today_mois=today_mois)
        elif utype == 2:
            client = user_infos[6]
            timesheets = get_submitted_timesheets_client(client)

            return render_template('dashboardClient.html',
                                   user_infos=user_infos,
                                   utype=utype, notifications=notifications,
                                   today_date=today_date,
                                   today_weekday=today_weekday,
                                   timesheets=timesheets)
        elif utype == 3:
            ordre_achat = Ordre_achat.afficher_oa_consultant(username,
                                                             db_session)
            timesheet = FeuilleDeTemps.afficher(ordre_achat.id, get_monday(),
                                                db_session)
            days_data = get_data_timesheet(timesheet)

            return render_template('dashboardConsultant.html',
                                   user_infos=user_infos,
                                   utype=utype, notifications=notifications,
                                   timesheet=timesheet, today_date=today_date,
                                   today_weekday=today_weekday,
                                   days_data=days_data)
        else:
            return None

# @Situation   : Gérer les clients
@app.route('/manageClients', methods=['GET', 'POST'])
def manage_clients():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    clients = Consultant.gestion_clients(username, db_session)

    clients_accounts = []

    for x in clients:
        client_name = str(x)
        client_name = client_name.replace('(\'', '')
        client_name = client_name.replace('\',)', '')

        clients_accounts.append(Client.afficher_client(client_name, db_session))

    return render_template('manageClients.html', user_infos=user_infos,
                           utype=utype, clients_accounts=clients_accounts)

# @Situation   : Gérer les consultants
@app.route('/manageConsultants', methods=['GET', 'POST'])
def manage_consultants():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    consultants = Client.gestion_consultants(user[0][6], db_session)

    consultants_email = []
    for x in consultants:
        consultant_email = str(x)
        consultant_email = consultant_email.replace('(\'', '')
        consultant_email = consultant_email.replace('\',)', '')

        consultants_email.append(consultant_email)

    consultants_accounts = [get_db().get_user_all_infos(each)[0] for each
                            in consultants_email]

    return render_template('manageConsultants.html', user_infos=user_infos,
                           consultants_accounts=consultants_accounts,
                           utype=utype)

# @Situation   : Gérer les contacts
@app.route('/manageContacts/<client>', methods=['GET', 'POST'])
def clientContacts(client):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    contacts_account = Contact.gestion_contacts(client, db_session)

    return render_template('manageContacts.html', user_infos=user_infos,
                           utype=utype, contacts_account=contacts_account,
                           account_type="Clients")

# @Situation   : Gérer les ressources
@app.route('/manageRessources', methods=['GET', 'POST'])
def manage_ressources():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        return render_template('manageRessources.html', user_infos=user_infos,
                               utype=utype)

# @Situation   : Gérer les comptes
@app.route('/manageAccounts', methods=['GET', 'POST'])
def manage_accounts():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    return render_template('manageAccounts.html', user_infos=user_infos,
                           utype=utype)

# @Situation   : Journal d'activité
@app.route('/logActivity', methods=['GET', 'POST'])
def log_activity():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        return render_template('logConsultant.html', user_infos=user_infos,
                               utype=utype)

# @Situation   : Messagerie
@app.route('/inbox', methods=['GET', 'POST'])
def inbox():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]
        
        notifications = Notification.afficher_notif_receveur(username, db_session)

        notifications.sort(key=lambda n: n.date, reverse = True)

        return render_template('inbox.html', user_infos=user_infos,
                               utype=utype, notifications=notifications)


@app.route('/getNewNotification/<user>', methods=['GET', 'POST'])
def getNewNotification(user):
    notification = Notification.afficher_notif_etat(user, "non lu", db_session)

    if notification:
        return "True"
    else:
        return "False"


@app.route('/getNewNotificationDashboard/<user>', methods=['GET', 'POST'])
def getNewNotificationDashboard(user):
    notifications = Notification.afficher_toute_notifs_non_lu(user, db_session)
    return render_template('dashboardInbox.html',
                           notifications=notifications)


@app.route('/readNotification/<notif_id>', methods=['GET', 'POST'])
def readNotification(notif_id):
    notification = Notification.afficher_notif_id(notif_id, db_session)
    notification.etat = "lu"
    db_session.commit()

    return "lue"


# @Situation   : Feuilles de temps redirection consultant
@app.route('/myTimeSheets', methods=['GET', 'POST'])
def myTimeSheet():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    return redirect('/myTimeSheets/' + username)

# @Situation   : Toutes les Feuilles de temps consultant
@app.route('/myTimeSheets/<username>', methods=['GET', 'POST'])
def myTimeSheets(username):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    ordre_achat = Ordre_achat.afficher_oa_consultant(username, db_session)

    ts_list = get_all_timesheets_consultant(ordre_achat)

    return render_template('timeSheetsConsultant.html', user_infos=user_infos,
                           utype=utype, timesheets=ts_list, username=username)


# @Situation   : Toutes les Feuilles de temps consultant
@app.route('/myTimeSheets/<username>/<status>', methods=['GET', 'POST'])
def myTimeSheetsUsernameStatus(username, status):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    ordre_achat = Ordre_achat.afficher_oa_consultant(username, db_session)

    if status == "InProgress":
        timesheets = get_now_timesheets_consultant(ordre_achat)
    elif status == "Pending":
        timesheets = get_pending_timesheets_consultant(ordre_achat)
    elif status == "Submitted":
        timesheets = get_submitted_timesheets_consultant(ordre_achat)
    elif status == "All":
        timesheets = get_all_timesheets_consultant(ordre_achat)
    else:
        return render_template('404.html', infos_user=None), 404

    return render_template('timeSheetsConsultant.html', user_infos=user_infos,
                           utype=utype, timesheets=timesheets,
                           username=username, status=status)

# @Situation   : Une Feuille de temps spécifique
@app.route('/myTimeSheet/<ordre_achat>/<date_debut>', methods=['GET', 'POST'])
def myTimeSheetOrdreAchatDateDebut(ordre_achat, date_debut):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        timesheet = FeuilleDeTemps.afficher(ordre_achat, date_debut,
                                            db_session)
        if timesheet is not None:
            list_days = get_days_timesheet(timesheet.date_debut,
                                           timesheet.date_fin)

            days_data = get_data_timesheet(timesheet)
            soumission = Consultant.afficher_soumission(username, db_session)
            taux_horaire = soumission.taux_horaire
            date = datetime.strptime(date_debut, '%Y-%m-%d')
            prev_timesheet = (date - timedelta(7)).date()
            next_timesheet =  (date + timedelta(7)).date()

            return render_template('timeSheetConsultant.html', user_infos=user_infos,
                                   utype=utype, timesheet=timesheet,
                                   list_days=list_days, days_data=days_data,
                                   taux_horaire=taux_horaire,
                                   prev_timesheet=prev_timesheet,
                                   next_timesheet=next_timesheet)
        else:
            return redirect('/timeSheets')


# @Situation   : Soumettre une feuille de temps
@app.route('/submitTimeSheet/<ordre_achat>/<date_debut>',
           methods=['GET', 'POST'])
def submitTimeSheet(ordre_achat, date_debut):

    timesheet = FeuilleDeTemps.afficher(ordre_achat, date_debut, db_session)
    timesheet.etat = 'soumise'
    db_session.commit()

    ordre_achat = Ordre_achat.afficher_id(ordre_achat, db_session)
    soumission_id = ordre_achat.soumission_id
    soumission = Soumission.afficher_id(soumission_id, db_session)
    consultant = soumission.consultant

    emetteur = consultant
    receveur = timesheet.approbateur
    contexte = "Soumission d'une feuille de temps"
    contenu = ("Une nouvelle feuille de temps à été soumise par " + emetteur +
               " pour la periode du " + timesheet.date_debut + " au " +
               timesheet.date_fin)

    send_notification_submit(emetteur, receveur, contexte, contenu)

    return redirect('/myTimeSheets')

# @Situation   : Re-soumettre une feuille de temps
@app.route('/reSubmitTimeSheet/<ordre_achat>/<date_debut>',
           methods=['GET', 'POST'])
def reSubmitTimeSheet(ordre_achat, date_debut):

    timesheet = FeuilleDeTemps.afficher(ordre_achat, date_debut, db_session)
    timesheet.etat = 'soumise'
    db_session.commit()

    ordre_achat = Ordre_achat.afficher_id(ordre_achat, db_session)
    soumission_id = ordre_achat.soumission_id
    soumission = Soumission.afficher_id(soumission_id, db_session)
    consultant = soumission.consultant

    emetteur = consultant
    receveur = timesheet.approbateur
    contexte = "Re-Soumission d'une feuille de temps"
    contenu = ("Une feuille de temps à été re-soumise par " + emetteur +
               " pour la periode du " + timesheet.date_debut + " au " +
               timesheet.date_fin + "Veuillez vérifier les informations" +
               " afin d'approuver la feuille de temps !")

    send_notification_submit(emetteur, receveur, contexte, contenu)

    return redirect('/myTimeSheets')


# @Situation   : Approuver une feuille de temps
@app.route('/approveTimeSheet/<ordre_achat>/<date_debut>',
           methods=['GET', 'POST'])
def approveTimeSheet(ordre_achat, date_debut):

    timesheet = FeuilleDeTemps.afficher(ordre_achat, date_debut, db_session)
    timesheet.etat = 'approuvee'
    db_session.commit()

    ordre_achat = Ordre_achat.afficher_id(ordre_achat, db_session)
    soumission_id = ordre_achat.soumission_id
    soumission = Soumission.afficher_id(soumission_id, db_session)

    emetteur = timesheet.approbateur
    receveur = soumission.consultant
    contexte = "Approbation d'une feuille de temps"
    contenu = ("La feuille de temps pour la periode du " + timesheet.date_debut + " au " +
               timesheet.date_fin + " à été approuvée !")

    send_notification_timesheet_state(emetteur, receveur, contexte, contenu)
    
    return redirect('/ConsultantTimeSheets')


# @Situation   : Approuver une feuille de temps
@app.route('/rejectTimeSheet/<ordre_achat>/<date_debut>',
           methods=['GET', 'POST'])
def rejectTimeSheet(ordre_achat, date_debut):
    reason = request.form["reason"]

    timesheet = FeuilleDeTemps.afficher(ordre_achat, date_debut, db_session)
    timesheet.etat = 'rejetee'
    db_session.commit()

    ordre_achat = Ordre_achat.afficher_id(ordre_achat, db_session)
    soumission_id = ordre_achat.soumission_id
    soumission = Soumission.afficher_id(soumission_id, db_session)

    emetteur = timesheet.approbateur
    receveur = soumission.consultant
    contexte = "Rejet d'une feuille de temps"
    contenu = ("La feuille de temps pour la periode du " + timesheet.date_debut + " au " +
               timesheet.date_fin + " à été rejetée ! Veuillez la corriger afin de répondre aux critère suivants :\n\n" + reason + "\nMerci !")

    send_notification_timesheet_state(emetteur, receveur, contexte, contenu)

    return redirect('/ConsultantTimeSheets')

# @Situation   : Sauvegarder données horaire consultant
@app.route('/saveTimeSheet/<ordre_achat>/<date_debut>/<jour>/<heure>/<period>',
           methods=['GET', 'POST'])
def saveTimeSheetConsultant(ordre_achat, date_debut, jour, heure, period):

    if "id" in session:
        username = get_db().get_session(session["id"])

        date_debut = get_particular_monday(date_debut)
        feuille = FeuilleDeTemps.afficher(ordre_achat, date_debut, db_session)
        jour = str(jour)

        if jour == "jour_1":
            data = parse_day_data(feuille.jour_1)
            new_date = check_period(data, period, heure)
            feuille.jour_1 = new_date
            db_session.commit()
        elif jour == "jour_2":
            data = parse_day_data(feuille.jour_2)
            new_date = check_period(data, period, heure)
            feuille.jour_2 = new_date
            db_session.commit()
        elif jour == "jour_3":
            data = parse_day_data(feuille.jour_3)
            new_date = check_period(data, period, heure)
            feuille.jour_3 = new_date
            db_session.commit()
        elif jour == "jour_4":
            data = parse_day_data(feuille.jour_4)
            new_date = check_period(data, period, heure)
            feuille.jour_4 = new_date
            db_session.commit()
        elif jour == "jour_5":
            data = parse_day_data(feuille.jour_5)
            new_date = check_period(data, period, heure)
            feuille.jour_5 = new_date
            db_session.commit()
        elif jour == "jour_6":
            data = parse_day_data(feuille.jour_6)
            new_date = check_period(data, period, heure)
            feuille.jour_6 = new_date
            db_session.commit()
        elif jour == "jour_7":
            data = parse_day_data(feuille.jour_7)
            new_date = check_period(data, period, heure)
            feuille.jour_7 = new_date
            db_session.commit()
        else:
            return render_template('404.html', infos_user=None), 404

        return ('<i class="pe-7s-diskette faa-flash animated"></i>', 200)
    else:
        return send_unauthorized()


# @Situation   : Feuilles de temps redirection vue client
@app.route('/ConsultantTimeSheets', methods=['GET', 'POST'])
def consultantTimeSheets():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    return redirect('/ConsultantTimeSheets/' + username)

# @Situation   : Toutes les Feuilles de temps consultant, vue client
@app.route('/ConsultantTimeSheets/<username>', methods=['GET', 'POST'])
def consultantTimeSheetsUsername(username):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    consultants = Client.gestion_consultants(user[0][6], db_session)

    consultants_email = []
    for x in consultants:
        consultant_email = str(x)
        consultant_email = consultant_email.replace('(\'', '')
        consultant_email = consultant_email.replace('\',)', '')

        consultants_email.append(consultant_email)

    consultants_accounts = [get_db().get_user_all_infos(each)[0] for each
                            in consultants_email]

    return render_template('timeSheetsClient.html', user_infos=user_infos,
                           utype=utype, username=username,
                           consultants_accounts=consultants_accounts)


@app.route('/getNewTimesheetsDashboard', methods=['GET', 'POST'])
def getNewTimesheetsDashboard():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]

        client = user_infos[6]
        timesheets = get_submitted_timesheets_client(client)
        return render_template('dashboardTimesheets.html',
                               timesheets=timesheets)


# @Situation   : Feuilles de temps en cours consultant, vue client
@app.route('/listTimeSheetsNow/<username>', methods=['GET', 'POST'])
def listTimeSheetsNow(username):
    ordre_achat = Ordre_achat.afficher_oa_consultant(username, db_session)
    timesheets = get_now_timesheets_consultant(ordre_achat)

    return render_template('listTimeSheetsConsultantClient.html',
                           timesheets=timesheets, consultant=username,
                           status="InProgress")

# @Situation   : Feuilles de temps toutes consultant, vue client
@app.route('/listTimeSheetsAll/<username>', methods=['GET', 'POST'])
def listTimeSheetsAll(username):
    ordre_achat = Ordre_achat.afficher_oa_consultant(username, db_session)
    timesheets = get_all_timesheets_consultant(ordre_achat)

    return render_template('listTimeSheetsConsultantClient.html',
                           timesheets=timesheets, consultant=username,
                           status="All")

# @Situation   : Feuilles de temps soumises consultant, vue client
@app.route('/listTimeSheetsSubmitted/<username>', methods=['GET', 'POST'])
def listTimeSheetsSubmitted(username):
    ordre_achat = Ordre_achat.afficher_oa_consultant(username, db_session)
    timesheets = get_submitted_timesheets_consultant(ordre_achat)

    return render_template('listTimeSheetsConsultantClient.html',
                           timesheets=timesheets, consultant=username,
                           status="Submitted")

# @Situation   : Feuilles de temps en attente consultant, vue client
@app.route('/listTimeSheetsPending/<username>', methods=['GET', 'POST'])
def listTimeSheetsPending(username):
    ordre_achat = Ordre_achat.afficher_oa_consultant(username, db_session)
    timesheets = get_pending_timesheets_consultant(ordre_achat)

    return render_template('listTimeSheetsConsultantClient.html',
                           timesheets=timesheets, consultant=username,
                           status="Pending")

# @Situation   : Feuille de temps consultant, vue client
@app.route('/timeSheetsConsultant/<oa_id>/<date_debut>', methods=['GET', 'POST'])
def timeSheetsConsultantOrdreAchatDateDebut(oa_id, date_debut):
    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    timesheet = FeuilleDeTemps.afficher(oa_id, date_debut, db_session)
    
    if timesheet is not None:
        ordre_achat = Ordre_achat.afficher_id(oa_id, db_session)
        soumission_id = ordre_achat.soumission_id
        soumission = Soumission.afficher_id(soumission_id, db_session)
        taux_horaire = soumission.taux_horaire
        list_days = get_days_timesheet(timesheet.date_debut, timesheet.date_fin)
        days_data = get_data_timesheet(timesheet)
        date = datetime.strptime(date_debut, '%Y-%m-%d')
        prev_timesheet = (date - timedelta(7)).date()
        next_timesheet = (date + timedelta(7)).date()

        return render_template('timeSheetConsultantClient.html', utype=utype,
                               user_infos=user_infos, timesheet=timesheet,
                               list_days=list_days, days_data=days_data,
                               taux_horaire=taux_horaire,
                               prev_timesheet=prev_timesheet,
                               next_timesheet=next_timesheet)
    else:
        return redirect('/ConsultantTimeSheets')


# @Situation   : Mon compte
@app.route('/myAccount', methods=['GET', 'POST'])
def my_account():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        return render_template('userProfile.html', user_infos=user_infos,
                               utype=utype)

# @Situation   : Profil d'un utilisateur
@app.route('/profile/<profile_username>', methods=['GET', 'POST'])
def profile_user(profile_username):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    profile = get_db().get_user_all_infos(profile_username)
    profile_infos = profile[0]

    return render_template('profile.html', user_infos=user_infos,
                           utype=utype, profile_infos=profile_infos)

# @Situation   : Tout les comptes, tenant administrateur
@app.route('/manageAccountsAdmin/<type_of_accounts>', methods=['GET', 'POST'])
def manage_accounts_admin(type_of_accounts):

    if type_of_accounts == "recruiters":
        accounts = get_db().get_all_accounts_recruiters()
        account_type = "Recruteurs"
    elif type_of_accounts == "consultants":
        accounts = get_db().get_all_accounts_consultants()
        account_type = "Consultants"
    elif type_of_accounts == "clients":
        accounts = get_db().get_all_accounts_clients()
        account_type = "Clients"
    else:
        return render_template('404.html', infos_user=None), 404

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        return render_template('accountsList.html', accounts=accounts,
                               account_type=account_type, utype=utype,
                               user_infos=user_infos)

# @Situation   : Tout les comptes, tenant recruteur
@app.route('/manageAccounts/<type_of_accounts>', methods=['GET', 'POST'])
def manage_accounts_recruiters(type_of_accounts):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    if type_of_accounts == "consultants":
        accounts = Tenant.afficher_consultants(user_infos[4], db_session)
        account_type = "Consultants"
    elif type_of_accounts == "clients":
        accounts = Tenant.afficher_clients(user_infos[4], db_session)
        account_type = "Clients"
    else:
        return render_template('404.html', infos_user=None), 404

    return render_template('accountsListRecruiters.html', accounts=accounts,
                           account_type=account_type, utype=utype,
                           user_infos=user_infos)


# @Situation   : Page affichant des confirmations
# @Param       : Le type de la confirmation à afficher
@app.route('/confirmation/<type_of_confirmation>', methods=['GET', 'POST'])
def confirmation(type_of_confirmation):

    if "id" in session:
        #        username = get_db().get_session(session["id"])

        return render_template("confirmation.html", login=True,
                               type_conf=type_of_confirmation)
    else:
        return render_template("confirmation.html", login=False,
                               type_conf=type_of_confirmation)


# @Situation   : Déconnection de l'utilisateur
# @Wraps       : Authentification requise !
# @Return      : Suppression de la clé de session 'id'
@app.route('/logout', methods=['GET', 'POST'])
@authentication_required
def logout():
    id_session = session["id"]
    session.pop('id', None)
    get_db().delete_session(id_session)

    return render_template("index.html", info=info_1)

# Situation : Ressources
@app.route('/ressources/<account_email>/<atype>', methods=['GET', 'POST'])
def ressources(account_email, atype):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    return render_template('ressources.html', user_infos=user_infos,
                           utype=utype, account_email=account_email,
                           atype=atype)

# Situation : Nouvel Avenant
@app.route('/new/user/<user_type>', methods=['GET', 'POST'])
def new_user(user_type):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    return render_template('addUser.html', user_infos=user_infos,
                           utype=utype, user_type=user_type)

# Situation : Avenant
@app.route('/avenant/<account_email>/<rtype>', methods=['GET', 'POST'])
def avenant(account_email, rtype):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    if rtype == "consultant":

        avenant = Consultant.afficher_avenant(account_email, db_session)

        return render_template('avenant.html', avenant=avenant,
                               user_infos=user_infos, utype=utype,
                               account_email=account_email, rtype=rtype)
    elif rtype == "client":

        avenant = Client.afficher_avenant(account_email, db_session)

        return render_template('avenant.html', avenant=avenant,
                               user_infos=user_infos, utype=utype,
                               account_email=account_email, rtype=rtype)
    else:
        return None

# Situation : Nouvel Avenant
@app.route('/new/avenant/<account_email>/<rtype>', methods=['GET', 'POST'])
def new_avenant(account_email, rtype):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    ordres_achat = Ordre_achat.afficher(db_session)

    return render_template('addAvenant.html', user_infos=user_infos,
                           utype=utype, account_email=account_email,
                           rtype=rtype, ordres_achat=ordres_achat)

# Situation : Ajouter un Avenant
@app.route('/add/avenant/<account_email>/<rtype>', methods=['GET', 'POST'])
def add_avenant(account_email, rtype):

    date_avenant = request.form['date_avenant']
    date_fin = request.form['date_fin']
    ordre_achat = request.form['ordre_achat']
    contact = request.form['contact']

    avenant = Avenant(date_avenant=date_avenant, date_fin=date_fin,
                      ordre_achat=ordre_achat, contact=contact)

    Avenant.ajouter(avenant, db_session)

    return redirect(url_for('avenant', account_email=account_email,
                            rtype=rtype))

# Situation : Dotation
@app.route('/dotation/<account_email>/<rtype>', methods=['GET', 'POST'])
def dotation(account_email, rtype):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    if rtype == "consultant":

        dotation = Consultant.afficher_dotation(account_email, db_session)

        return render_template('dotation.html', dotation=dotation,
                               user_infos=user_infos, utype=utype,
                               account_email=account_email, rtype=rtype)
    elif rtype == "client":

        dotation = Client.afficher_dotation(account_email, db_session)

        return render_template('dotation.html', dotation=dotation,
                               user_infos=user_infos, utype=utype,
                               account_email=account_email, rtype=rtype)
    else:
        return None

# Situation : Nouvelle Dotation
@app.route('/new/dotation/<account_email>/<rtype>', methods=['GET', 'POST'])
def new_dotation(account_email, rtype):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    return render_template('addDotation.html', user_infos=user_infos,
                           utype=utype, account_email=account_email,
                           rtype=rtype)

# Situation : Ajouter une Dotation
@app.route('/add/dotation/<account_email>/<rtype>', methods=['GET', 'POST'])
def add_dotation(account_email, rtype):

    date_dotation = request.form['date_dotation']
    ordre_achat = request.form['ordre_achat']
    ouvert = request.form['ouvert']

    dotation = Dotation(date_dotation=date_dotation, ordre_achat=ordre_achat,
                        ouvert=ouvert)

    Dotation.ajouter(dotation, db_session)

    return redirect(url_for('dotation', account_email=account_email,
                            rtype=rtype))

# Situation : Ordre d'achat
@app.route('/ordre_achat/<account_email>/<rtype>', methods=['GET', 'POST'])
def ordre_achat(account_email, rtype):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    if rtype == "consultant":

        ordre_achat = Consultant.afficher_oa(account_email, db_session)

        return render_template('ordre_achat.html', ordre_achat=ordre_achat,
                               user_infos=user_infos, utype=utype,
                               account_email=account_email, rtype=rtype)
    elif rtype == "client":

        ordre_achat = Client.afficher_oa(account_email, db_session)

        return render_template('ordre_achat.html', ordre_achat=ordre_achat,
                               user_infos=user_infos, utype=utype,
                               account_email=account_email, rtype=rtype)
    else:
        return None

# Situation : Nouvel Ordre d'achat
@app.route('/new/ordre_achat/<account_email>/<rtype>', methods=['GET', 'POST'])
def new_ordre_achat(account_email, rtype):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    return render_template('addOrdre_achat.html', user_infos=user_infos,
                           utype=utype, account_email=account_email,
                           rtype=rtype)

# Situation : Ajouter un Ordre d'achat
@app.route('/add/ordre_achat/<account_email>/<rtype>', methods=['GET', 'POST'])
def add_ordre_achat(account_email, rtype):

    date_oa = request.form['date_oa']
    soumission_id = request.form['soumission_id']
    date_debut = request.form['date_debut']
    date_fin = request.form['date_fin']

    ordre_achat = Ordre_achat(date_oa=date_oa, soumission_id=soumission_id,
                              date_debut=date_debut, date_fin=date_fin)

    Ordre_achat.ajouter(ordre_achat, db_session)

    return redirect(url_for('ordre_achat', account_email=account_email,
                            rtype=rtype))

# Situation : Soumission
@app.route('/soumission/<account_email>/<rtype>', methods=['GET', 'POST'])
def soumission(account_email, rtype):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    if rtype == "consultant":

        soumission = Consultant.afficher_soumission(account_email, db_session)

        return render_template('soumission.html', soumission=soumission,
                               user_infos=user_infos, utype=utype,
                               account_email=account_email, rtype=rtype)
    elif rtype == "client":

        soumission = Client.afficher_soumission(account_email, db_session)

        return render_template('soumission.html', soumission=soumission,
                               user_infos=user_infos, utype=utype,
                               account_email=account_email, rtype=rtype)
    else:
        return None

# Situation : Nouvelle Soumission
@app.route('/new/soumission/<account_email>/<rtype>', methods=['GET', 'POST'])
def new_soumission(account_email, rtype):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

    return render_template('addSoumission.html', user_infos=user_infos,
                           utype=utype, account_email=account_email,
                           rtype=rtype)

# Situation : Ajouter une Soumission
@app.route('/add/soumission/<account_email>/<rtype>', methods=['GET', 'POST'])
def add_soumission(account_email, rtype):

    date_soumission = request.form['date_soumission']
    recruteur = request.form['recruteur']
    client = request.form['client']
    consultant = request.form['consultant']
    demande_id = request.form['demande_id']
    taux_horaire = request.form['taux_horaire']
    duree = request.form['duree']
    details = request.form['details']

    soumission = Soumission(date_soumission=date_soumission,
                            recruteur=recruteur, client=client,
                            consultant=consultant, demande_id=demande_id,
                            taux_horaire=taux_horaire, duree=duree,
                            details=details)

    Soumission.ajouter(soumission, db_session)

    return redirect(url_for('soumission', account_email=account_email,
                            rtype=rtype))


# Situation : Consulter son abonnement
@app.route('/subscription', methods=['GET', 'POST'])
def subscription():

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        entreprise = user_infos[4]
        factures = Facture.trouver_tenant(entreprise, db_session)
        transactions = Transaction.trouver_recruteur(username, db_session)
        abonnement_name = Abonnement.afficher_offre(entreprise, db_session)
        abonnement_name = convert_sql_alchemy_to_string(abonnement_name)
        
        abonnement = Abonnement.afficher(entreprise, db_session)
        offre = Offre.informations_offre(abonnement_name, db_session)
        
        facture_details = get_subscription(username)
        total_consultants = facture_details.get("nb_feuilles") / 4

        month_infos = {"month": get_mois(), "year": get_year()}

        
        return render_template('subscription.html', user_infos=user_infos,
                               utype=utype, factures=factures,
                               transactions=transactions,
                               offre=offre, abonnement=abonnement,
                               facture_details=facture_details,
                               total_consultants=total_consultants,
                               month_infos=month_infos)


# Situation : Modifier son abonnement
@app.route('/modifySubscription', methods=['GET', 'POST'])
def modSubscription():
    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        return render_template('modifySubscription.html', user_infos=user_infos,
                               utype=utype)


# Situation : Modifier son abonnement (form submit)
@app.route('/modifySubscription_submit/<entreprise>/<date_debut>/<offre>/<mensualite>',
           methods=['GET', 'POST'])
def modifySubscription_submit(entreprise, date_debut, offre, mensualite):
    abonnement = Abonnement.afficher(entreprise, db_session)
    abonnement.date_debut = date_debut

    date_fin = datetime.strptime(date_debut, '%Y-%m-%d') + relativedelta(months=int(mensualite))
    date_fin = date_fin.strftime("%Y-%m-%d")
    abonnement.date_fin = date_fin
    abonnement.offre = offre
    db_session.commit()

    return render_template('confirmation.html', type_conf="modifySubscriptionSuccess")


# Situation : Annuler son abonnement
@app.route('/cancelSubscription', methods=['GET', 'POST'])
def cancelSubscription():
    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        abonnement = Abonnement.afficher(user_infos[4], db_session)
        factures = Facture.trouver_non_payee(user_infos[4], db_session)
        total_nb_factures = len(factures)
        
        total_factures = 0
        for i in range (0, len(factures)):
            total_factures = total_factures + factures[i].sous_total
        
        return render_template('cancelSubscription.html',
                               user_infos=user_infos,
                               utype=utype, factures=factures,
                               abonnement=abonnement,
                               total_nb_factures=total_nb_factures,
                               total_factures=total_factures)


# Situation : Annuler son abonnement (form submit)
@app.route('/cancelSubscription_submit', methods=['GET', 'POST'])
def cancelSubscription_submit():

    password = request.form["password"]

    username = get_db().get_session(session["id"])
    user = get_db().get_user_all_infos(username)
    user_infos = user[0]

    salt = user_infos[5]

    hashed_password = (hashlib.sha512(str(password + salt).encode("utf-8"))
                       .hexdigest())

    if hashed_password == user_infos[6]:
        abonnement = Abonnement.afficher(user_infos[4], db_session)
        abonnement.etat = "clos"
        db_session.commit()

        return render_template('confirmation.html',
                               type_conf="cancelSubscriptionSuccess"), 200
    else:
        return "Le mot de passe est incorrect !", 401

# Situation : Renouveller son abonnement
@app.route('/renewSubscription', methods=['GET', 'POST'])
def renewSubscription():
    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        abonnement = Abonnement.afficher(user_infos[4], db_session)

        return render_template('renewSubscription.html', user_infos=user_infos,
                               utype=utype, abonnement=abonnement)


# Situation : Renouveller son abonnement (form submit)
@app.route('/renewSubscription_submit/<entreprise>/<date_debut>/<mensualite>',
           methods=['GET', 'POST'])
def renewSubscription_submit(entreprise, date_debut, mensualite):
    abonnement = Abonnement.afficher(entreprise, db_session)
    abonnement.date_debut = date_debut

    date_fin = datetime.strptime(date_debut, '%Y-%m-%d') + relativedelta(months=int(mensualite))
    date_fin = date_fin.strftime("%Y-%m-%d")
    abonnement.date_fin = date_fin
    db_session.commit()

    return render_template('confirmation.html', 
                           type_conf="renewSubscriptionSuccess")

# Situation : Consulter une facture
@app.route('/bill/<id_facture>', methods=['GET', 'POST'])
def bill(id_facture):

    if "id" in session:
        username = get_db().get_session(session["id"])
        user = get_db().get_user_all_infos(username)
        user_infos = user[0]
        utype = user[1]

        entreprise = user_infos[4]
        facture = Facture.trouver_par_id_tenant(id_facture, entreprise, db_session)
        transaction = Transaction.trouver_facture(id_facture, db_session)        
        total_consultants = Consultant.get_total_consultant_par_recruteur(entreprise, db_session) / 4
        
        if facture:
            str_mois = list_month_fr[facture.mois-1]

            return render_template('billSubscription.html', user_infos=user_infos,
                                   utype=utype, facture=facture,
                                   transaction=transaction,
                                   str_mois=str_mois,
                                   total_consultants=total_consultants)
        else:
            return render_template('404.html'), 404


# Situation : Ajouter une transaction
@app.route('/addTransaction/<recruteur>/<date>/<montant>/<id_paypal>/<status>', methods=['GET', 'POST'])
def add_transaction(recruteur, date, montant, id_paypal, status):
    
    id_facture = Facture.trouver_id_par_date_montant(date, montant, db_session)
    id_facture = convert_sql_alchemy_to_int(id_facture)

    transaction = Transaction(id_paypal=id_paypal, recruteur=recruteur,
                              date=date[0:10], id_facture=id_facture,
                              montant=montant, status=status)

    Transaction.ajouter(transaction, db_session)
    Facture.update_status(id_facture, db_session)

    return "true", 200


# Situation : Payer une facture
@app.route('/payBill/<entreprise>/<date_facture>/<mois>/<annee>/<mensualite>/<nbr_feuille>/<cout_feuille>/<sous_total>/<etat>/<details>', methods=['GET', 'POST'])
def pay_bill(entreprise, date_facture, mois, annee, mensualite,
             nbr_feuille, cout_feuille, sous_total, etat, details):

    facture = Facture(entreprise=entreprise, date_facture=date_facture, mois=mois , annee=annee,                           mensualite=mensualite, nbr_feuille=nbr_feuille, cout_feuille=cout_feuille,                           sous_total=sous_total, etat=etat, details=details)

    Facture.ajouter(facture, db_session)

    return "true", 200


# Situation : Payer toute les factures
@app.route('/payAllBill/<recruteur>/<entreprise>/<date>/<montant>/<id_paypal>/<status>', methods=['GET', 'POST'])
def pay_all_bill(recruteur, entreprise, date, montant, id_paypal, status):
    
    factures = Facture.trouver_non_payee(entreprise, db_session)
    for i in range(0, len(factures)):
        factures[i].etat = "payee"
        db_session.commit()

    facture = Facture(entreprise=entreprise, date_facture=date, mois=0 , annee=date[0:4],                                   mensualite=montant, nbr_feuille=0, cout_feuille=0, sous_total=montant,
                      etat="non payee", details="Annulation de l'abonnement, paiement de toute les factures")
    Facture.ajouter(facture, db_session)

    id_facture = Facture.trouver_id_par_date_montant(date, montant, db_session)
    id_facture = convert_sql_alchemy_to_int(id_facture)

    transaction = Transaction(id_paypal=id_paypal, recruteur=recruteur,
                              date=date, id_facture=id_facture,
                              montant=montant, status=status)

    Transaction.ajouter(transaction, db_session)
    Facture.update_status(id_facture, db_session)

    return "true", 200


# @Description : Paramétrer et envoyer des courriels
# @Param       : la source, les destinataire, le sujet, le message et le
#              | destinataire en copie
def send_email(source, destination, sujet, message, copie):
    source_address = source
    body = message
    subject = sujet

    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = source_address
    msg['To'] = destination

    if copie is not None:
        msg['Cc'] = copie
        destination_address = [destination] + [copie]
    else:
        destination_address = [destination]

    msg.attach(MIMEText(body, 'html'))

    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(source_address, 'coopama2019')
    text = msg.as_string()
    server.sendmail(source_address, destination_address, text)
    server.quit()


def send_notification_submit(emetteur, receveur, contexte, contenu):
    date = get_date()
    ordre_achat = Ordre_achat.afficher_oa_consultant(emetteur, db_session)
    notification = Notification(ordre_achat_id=ordre_achat.id, emetteur=emetteur,
                                receveur=receveur, date=date, contexte=contexte,
                                contenu=contenu, etat='non lu')

    Notification.ajouter(notification, db_session)


def send_notification_timesheet_state(emetteur, receveur, contexte, contenu):
    date = get_date()
    ordre_achat = Ordre_achat.afficher_oa_consultant(receveur, db_session)
    notification = Notification(ordre_achat_id=ordre_achat.id, emetteur=emetteur,
                                receveur=receveur, date=date, contexte=contexte,
                                contenu=contenu, etat='non lu')

    Notification.ajouter(notification, db_session)

# ----- FONCTIONS UTILES -----

# @Description : Vérifier si l'utilisateur est connecté
# @Param       : Session du navigateur
# @Return      : la valeur de la clé 'id' dans les sessions => courriel
def is_authenticated(session):
    return "id" in session


# @Situation   : Utilisateur non authentifié
def send_unauthorized():
    return (redirect('/'),
            401)


def get_date():
    jour = datetime.now()

    return jour.strftime('%Y-%m-%d %H:%M:%S')

def get_mois():
    jour = datetime.today()
    
    mois = jour.month
    return list_month_fr[mois-1]

def get_year():
    jour = datetime.today()
    
    annee = jour.year
    return annee
    
def get_monday():
    lastMonday = date.today()
    oneday = timedelta(days=1)

    while lastMonday.weekday() != calendar.MONDAY:
        lastMonday -= oneday

    return lastMonday.strftime("%Y-%m-%d")


def get_first_day_current_month():
    today = datetime.today()
    ce_mois = str(date(today.year, today.month, 1))

    return ce_mois


def get_particular_monday(date_save):
    lastMonday = datetime.strptime(date_save, "%Y-%m-%d %H:%M:%S")
    oneday = timedelta(days=1)

    while lastMonday.weekday() != calendar.MONDAY:
        lastMonday -= oneday

    return lastMonday.strftime("%Y-%m-%d")


def get_days_timesheet(date_debut, date_fin):
    d1 = datetime.strptime(date_debut, '%Y-%m-%d')
    d2 = datetime.strptime(date_fin, '%Y-%m-%d')
    diff = d2 - d1
    list_days = []

    for i in range(diff.days + 1):
        dates = d1 + timedelta(i)
        list_days.append({"name": days_week[dates.weekday()],
                          "date": d1 + timedelta(i),
                          "month": list_month_fr[dates.month-1]})

    return list_days


def get_data_timesheet(timesheet):
    data = []

    day_1 = parse_day_data(timesheet.jour_1)
    day_2 = parse_day_data(timesheet.jour_2)
    day_3 = parse_day_data(timesheet.jour_3)
    day_4 = parse_day_data(timesheet.jour_4)
    day_5 = parse_day_data(timesheet.jour_5)
    day_6 = parse_day_data(timesheet.jour_6)
    day_7 = parse_day_data(timesheet.jour_7)

    data.append(day_1)
    data.append(day_2)
    data.append(day_3)
    data.append(day_4)
    data.append(day_5)
    data.append(day_6)
    data.append(day_7)

    return data


def parse_day_data(day_data):
    am_pm = day_data.split('|')
    am = am_pm[0].split('-')
    pm = am_pm[1].split('-')

    day = {"AM_s": am[0].strip(), "AM_e": am[1].strip(), "PM_s": pm[0].strip(),
           "PM_e": pm[1].strip()}

    return day


def check_period(data, period, heure):
    am_s = str(data['AM_s'])
    am_e = str(data['AM_e'])
    pm_s = str(data['PM_s'])
    pm_e = str(data['PM_e'])

    heure = str(heure).strip()

    if period == "AMS":
        new_date = heure + " - " + am_e + " | " + pm_s + " - " + pm_e
    elif period == "AME":
        new_date = am_s + " - " + heure + " | " + pm_s + " - " + pm_e
    elif period == "PMS":
        new_date = am_s + " - " + am_e + " | " + heure + " - " + pm_e
    else:
        new_date = am_s + " - " + am_e + " | " + pm_s + " - " + heure

    return new_date


def convert_sql_alchemy_to_string(string):
    string = str(string)
    string = string.replace("('", "")
    string = string.replace("',)", "")
    
    return string

def convert_sql_alchemy_to_int(number):
    number = str(number)
    number = number.replace('(', '')
    number = number.replace(',)', '')

    return int(number)

def convert_sql_alchemy_to_float(number):
    number = str(number)
    number = number.replace('(', '')
    number = number.replace(',)', '')
    
    return float(number)

def get_all_timesheets_consultant(ordre_achat):
    date_start = date(int(ordre_achat.date_debut[0:4]),
                      int(ordre_achat.date_debut[5:7]),
                      int(ordre_achat.date_debut[8:10]))

    date_end = date(int(ordre_achat.date_fin[0:4]),
                    int(ordre_achat.date_fin[5:7]),
                    int(ordre_achat.date_fin[8:10]))

    ts_list = []

    while date_start < date_end:
        start_of_week = date_start - timedelta(days=date_start.weekday())
        end_of_week = start_of_week + timedelta(days=6)
        ts_list.append(FeuilleDeTemps.afficher(ordre_achat.id, start_of_week, db_session))
        date_start = end_of_week + timedelta(days=1)

    return ts_list


def get_now_timesheets_consultant(ordre_achat):
    timesheets = [FeuilleDeTemps.afficher(ordre_achat.id, get_monday(), db_session)]
    return timesheets


def get_submitted_timesheets_consultant(ordre_achat):
    soumise = FeuilleDeTemps.afficher_par_status(ordre_achat.id, "soumise", db_session)

    approuvee = FeuilleDeTemps.afficher_par_status(ordre_achat.id, "approuvee", db_session)

    rejetee = FeuilleDeTemps.afficher_par_status(ordre_achat.id, "rejetee", db_session)

    timesheets = soumise + approuvee + rejetee

    return timesheets


def get_pending_timesheets_consultant(ordre_achat):
    etat_none = FeuilleDeTemps.afficher_par_status(ordre_achat.id, None, db_session)

    etat_attente = FeuilleDeTemps.afficher_par_status(ordre_achat.id, "attente", db_session)

    timesheets = etat_none + etat_attente

    return timesheets


def get_submitted_timesheets_client(client):
    soumissions_client = Soumission.afficher_soumission_id_par_client(
        client, db_session)
    soumissions_client = [str(soumission) for soumission
                          in soumissions_client]

    soumission_id = []
    for soumission in soumissions_client:
        soum_id = soumission.replace('(', ' ')
        soum_id = soum_id.replace(',)', ' ')
        soumission_id.append(soum_id)

    ordre_achat_client = []
    for soum_id in soumission_id:
        oa_id = Ordre_achat.afficher_id_par_soumission_id(soum_id,
                                                          db_session)
        oa_id = str(oa_id)
        ordre_achat_client.append(oa_id)

    monday_week_now = get_monday()

    ordre_achat_id = []
    for ordre_achat in ordre_achat_client:
        oa_id = ordre_achat.replace('[(', ' ')
        oa_id = oa_id.replace(',)]', ' ')
        ordre_achat_id.append(oa_id)

    timesheets = []
    for oa_id in ordre_achat_id:
        timesheets_submitted = FeuilleDeTemps.afficher_par_status(oa_id, 'soumise',
                                            db_session)
        if timesheets_submitted is not None:
            for timesheet in timesheets_submitted:
                soumission_id = (Ordre_achat.afficher_id(
                                    oa_id, db_session)).soumission_id
                consultant_email = str(Soumission.afficher_consultant(
                                        soumission_id, db_session))
                consultant_email = consultant_email.replace("('", "")
                consultant_email = consultant_email.replace("',)", "")
                consultant_infos = Consultant.afficher(consultant_email,
                                                       db_session)
                timesheets.append({"consultant": consultant_infos,
                                   "timesheet": timesheet})
        else:
            continue

    return timesheets


def get_subscription(username):
    list_oa = Ordre_achat.lister_oa_id_par_recruteur(username, db_session)
    total_fdt_month = 0

    for i in range(len(list_oa)):
        oa = convert_sql_alchemy_to_int(list_oa[i])
        nb_feuille = FeuilleDeTemps.mois_courant(oa, db_session)
        total_fdt_month = total_fdt_month + nb_feuille

    tenant = Recruteur.afficher_tenant(username, db_session)
    tenant = convert_sql_alchemy_to_string(tenant)

    mensualite = Abonnement.afficher_mensualite(tenant, db_session)
    mensualite = convert_sql_alchemy_to_int(mensualite)
    coutFeuille = Abonnement.afficher_cout_feuille(tenant, db_session)
    coutFeuille = convert_sql_alchemy_to_float(coutFeuille)        
    max_consultant = Abonnement.afficher_max_consultant(tenant, db_session)
    max_consultant = convert_sql_alchemy_to_int(max_consultant)        
    forfait = Abonnement.afficher_offre(tenant, db_session)
    forfait = convert_sql_alchemy_to_string(forfait)  
    total_consultant = Consultant.get_total_consultant_par_recruteur(tenant, db_session)
    total_consultant = convert_sql_alchemy_to_int(total_consultant)
    nombre_mois_abo = Abonnement.afficher_mois_total_abonnement(tenant, db_session)
    totalNbMois = Abonnement.afficher_mois_utilise_abonnement(tenant, db_session)
    sous_total_feuille = total_fdt_month * coutFeuille
    total = mensualite + (total_fdt_month * coutFeuille)


    mensualite = format(mensualite, '.2f')
    sous_total_feuille = format(sous_total_feuille, '.2f')
    total = format(total, '.2f')

    subscription = {"nb_feuilles": total_fdt_month, "mensualite": mensualite, "max_consultant":                            max_consultant, "totalMois": total, "totalNbMois": totalNbMois,                                      "totalFeuilles": sous_total_feuille, "name": forfait, "nb_mois_abo":                                  nombre_mois_abo}

    return subscription

def get_details_dashboard(username):
    tenant = Recruteur.afficher_tenant(username, db_session)
    tenant = convert_sql_alchemy_to_string(tenant)

    total_consultants = Consultant.get_total_consultant_par_recruteur(tenant, db_session)
    total_clients = Client.get_total_client_par_recruteur(tenant, db_session)

    details = {"totalConsultants": total_consultants, "totalClients": total_clients}

    return details

# AMAConsulting/Tenant.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer
from .base import Base
from .Demande import Demande
from .Contact import Contact
from .Soumission import Soumission
from .Recruteur import Recruteur
from .Client import Client
from .Consultant import Consultant


class Tenant(Base):
    __tablename__ = 'tenant'
    entreprise = Column(String, primary_key=True)
    email = Column(String)
    telephone = Column(String)

    def __repr__(self):
        return "<Tenant(entreprise='%s', email='%s', telephone='%s')>" % (self.entreprise, self.email, self.telephone)

    @staticmethod
    def afficher_recruteur(tenant, session):
        for instance in session.query(Recruteur).filter(
            Recruteur.email.in_(session.query(Recruteur.email).filter(Recruteur.entreprise == tenant))):
            print(instance)

    @staticmethod
    def afficher_client(tenant, session):
        for instance in session.query(Client).filter(
            Client.entreprise.in_(session.query(Client.entreprise).filter(Client.entreprise == tenant))):
            print(instance)

    def afficher_consultant(tenant, session):
        for instance in session.query(Consultant).filter(
            Consultant.entreprise.in_(session.query(Consultant.entreprise).filter(Consultant.entreprise == tenant))):
            return instance

    def afficher_consultants(tenant, session):
        consultants = [instance for instance in session.query(
            Consultant).filter(Consultant.entreprise.in_(
            session.query(Consultant.entreprise).filter(
            Consultant.entreprise == tenant)))]
        return consultants

    def afficher_clients(tenant, session):
        clients = [instance for instance in session.query(Client).filter(
            Client.entreprise.in_(session.query(Client.entreprise).filter(
            Client.entreprise == tenant)))]
        return clients


    @staticmethod
    def afficher_tenants(session):
        for instance in session.query(Tenant):
            print(instance)
    
    @staticmethod
    def afficher_demande(tenant, session):
        for instance in session.query(Demande).filter(
            Demande.contact.in_(session.query(Contact.email).filter(Contact.entreprise == tenant))):
            print(instance)

    @staticmethod
    def afficher_soumission(tenant, session):
        for instance in session.query(Soumission).filter(Soumission.demande_id.in_(session.query(Demande.id).filter(
            Demande.contact.in_(session.query(Contact.email).filter(Contact.entreprise == tenant))))):
            print(instance)

    @staticmethod
    def afficher_dotation(tenant,session):
        for instance in session.query(Dotation).filter(Dotation.ordre_achat.in_(
            session.query(Soumission.demande_id).filter(Soumission.demande_id.in_(session.query(Demande.id).filter(
                Demande.contact.in_(session.query(Contact.email).filter(Contact.entreprise == tenant))))))):
            print(instance)

    def ajouter_multi(tenants, session):
        for tenant in tenants:
            session.add(tenant)
            session.commit()
            
    def ajouter(tenant, session):
        session.add(tenant)
        session.commit()
# AMAConsulting/Recruteur.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey, create_engine, Boolean
from .base import Base
from .Soumission import Soumission


class Recruteur(Base):
    __tablename__ = 'recruteur'
    nom = Column(String)
    prenom = Column(String)
    email = Column(String, primary_key=True)
    telephone = Column(String)
    entreprise = Column(String,ForeignKey('tenant.entreprise'))
    salt = Column(String)
    hash = Column(String)

    def __repr__(self):
        return "<Recruteur(nom='%s', prenom='%s', email='%s', telephone='%s', salt='%s', entreprise='%s')>" % (self.nom, self.prenom, self.email, self.telephone, self.entreprise )

    def ajouter_multi(recruteurs, session):
        for recruteur in recruteurs:
            session.add(recruteur)
    
    def ajouter(recruteur, session):
        session.add(recruteur)
        session.commit()

    def afficher_tenant(email, session):
        for instance in session.query(Recruteur.entreprise).filter(Recruteur.email == email):
            return instance
--
-- File generated with SQLiteStudio v3.2.1 on Mon Apr 8 14:35:32 2019
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: offre
CREATE TABLE offre (
    type           TEXT    CHECK (type IN ('bronze', 'argent', 'or') ) 
                           NOT NULL
                           UNIQUE
                           PRIMARY KEY,
    mensualite     INTEGER CHECK (mensualite IN ('10', '20', '35') ),
    cout_feuille   INTEGER CHECK (cout_feuille IN ('1', '0.8', '0.7') ),
    max_consultant INTEGER CHECK (max_consultant IN ('99999', '25', '10') ) 
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;

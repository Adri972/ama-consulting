# AMA Consulting - Logiciel de gestion de ressources

## Configuration requise (dans l'ordre) :  

1. **Python 3.7.x** : [Download](https://www.python.org/downloads/release/python-372/)
    + Cocher "_Add python 3.7 to PATH_" lors de l'installation

2. **SQLite3 3.26.0** : [Download](sqlite-tools-win32-x86-3260000.zip)
    + Créer un nouveau dossier : ```C:\sqlite```
    + Extraire le contenu de l'archive téléchargé dans ce dossier
    + Ajouter ```C:\sqlite``` aux variables d'environnment **_PATH_** de votre système

3. **Mettre à jour pip (python)** : ```$>python -m pip install --upgrade pip```

4. **Flask** : ```$>pip install flask```

_Optionnel : Outils de vérification syntaxique :_

5. **Pep8 et flake8** : ```$>pip install pep8``` et ```$>pip install flake8```

6. **Pylint** : ```$>pip install pylint```

7. **Python dateutil** : ```$>pip install python-dateutil```

8. **SQLAlchemy** : ```$>pip install sqlalchemy```

## Lancer la visualisation de l'application :

1. ```$> python config.py``` (Pas nécéssaire au début, à configurer plus tard)

2. ```$> set FLASK_APP=index```

3. ```$> flask run```

4. Aller à [127.0.0.1:5000](127.0.0.1:5000)

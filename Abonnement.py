# AMAConsulting/Abonnement.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey
from .base import Base
from .Offre import Offre
from datetime import datetime, date



class Abonnement(Base):
    __tablename__ = 'abonnement'
    entreprise = Column(String, ForeignKey('tenant.entreprise'), primary_key=True)
    date_debut = Column(String)
    date_fin = Column(String)
    offre = Column(String,ForeignKey('offre.type'))
    etat = Column(String)

    def __repr__(self):
        return "<Abonnement(entreprise='%s', date_debut='%s', date_fin='%s', offre='%s', etat=='%s')>" \
            % (self.entreprise, self.date_debut, self.date_fin, self.offre, self.etat)

    def ajouter_multi(abonnements, session):
        for abonnement in abonnements:
            session.add(abonnement)
            session.commit()
            
    def ajouter(abonnement, session):
        session.add(abonnement)
        session.commit()

    def afficher(entreprise, session):
        for instance in session.query(Abonnement).filter(Abonnement.entreprise == entreprise):
            return instance

    def afficher_mensualite(entreprise, session):
        return session.query(Offre.mensualite).filter(Abonnement.entreprise == entreprise).filter(Abonnement.offre == Offre.type).first()

    def afficher_cout_feuille(entreprise, session):
        return  session.query(Offre.cout_feuille).filter(Abonnement.entreprise == entreprise).filter(Abonnement.offre == Offre.type).first()

    def afficher_max_consultant(entreprise, session):
        return session.query(Offre.max_consultant).filter(Abonnement.entreprise == entreprise).filter(Abonnement.offre == Offre.type).first()

    def afficher_offre(entreprise, session):
        return session.query(Abonnement.offre).filter(Abonnement.entreprise == entreprise).first()

    def afficher_mois_total_abonnement(entreprise, session):
        date1 = session.query(Abonnement.date_debut).filter(Abonnement.entreprise == entreprise).first()
        date1 = str(date1)
        date1 = date1.replace("('", "")
        date1 = date1.replace("',)","")
        date1 = datetime.strptime(date1, '%Y-%m-%d')
        date2 = session.query(Abonnement.date_fin).filter(Abonnement.entreprise == entreprise).first()
        date2 = str(date2)
        date2 = date2.replace("('", "")
        date2 = date2.replace("',)","")
        date2 = datetime.strptime(date2, '%Y-%m-%d')
        
        delta = date2 - date1
        
        return int(delta.days / 30)
    
    def afficher_mois_utilise_abonnement(entreprise, session):
        date1 = session.query(Abonnement.date_debut).filter(Abonnement.entreprise == entreprise).first()
        date1 = str(date1)
        date1 = date1.replace("('", "")
        date1 = date1.replace("',)","")
        date1 = datetime.strptime(date1, '%Y-%m-%d')
        date2 = datetime.today()
        date2 = str(date(date2.year, date2.month, date2.day))
        date2 = datetime.strptime(date2, '%Y-%m-%d')
        
        delta = date2 - date1
        
        return int(delta.days / 30)

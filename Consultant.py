# AMAConsulting/Consultant.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from .base import Base
from .Soumission import Soumission
from .Ordre_achat import Ordre_achat
from .Dotation import Dotation
from .Avenant import Avenant

class Consultant(Base):
    __tablename__ = 'consultant'
    id = Column(Integer, primary_key=True)
    prenom = Column(String)
    nom = Column(String)
    email = Column(String)
    telephone = Column(String)
    adresse = Column(String)
    ville = Column(String)
    province = Column(String)
    code_postal = Column(String)
    salt = Column(String)
    hash = Column(String)
    disponible = Column(Boolean)
    entreprise = Column(String, ForeignKey('tenant.entreprise'))

    def __repr__(self):
        return "<Consultant( nom='%s', prenom='%s', email='%s', telephone='%s', adresse='%s', ville='%s', province='%s', code_postal='%s', disponible='%s', entreprise='%s')>" \
            % (self.prenom, self.nom, self.email, self.telephone,
               self.disponible, self.entreprise)

    def afficher(consultant, session):
        for instance in session.query(Consultant).filter(Consultant.email == consultant):
            return instance

    def afficher_soumission(consultant, session):
        for instance in session.query(Soumission).filter(Soumission.consultant == consultant):
            return instance

    def afficher_oa(consultant, session):
        for instance in session.query(Ordre_achat).filter(Ordre_achat.soumission_id.is_(session.query(Soumission.id).filter(Soumission.consultant == consultant))):
            return instance

    def afficher_dotation(consultant, session):
        for instance in session.query(Dotation).filter(Dotation.ordre_achat.is_(session.query(Soumission.id).filter(Soumission.consultant == consultant))):
            return instance

    def afficher_avenant(consultant, session):
        for instance in session.query(Avenant).filter(Avenant.ordre_achat.is_(session.query(Soumission.id).filter(Soumission.consultant == consultant))):
            return instance

    def ajouter_multi(consultants, session):
        for consultant in consultants:
            session.add(consultant)

    def ajouter(consultant, session):
        session.add(consultant)
        session.commit()

    def gestion_clients(consultant, session):
        clients = [instance for instance in session.query(
            Soumission.client).filter(Soumission.consultant == consultant)]
        return clients

    def get_total_consultant_par_recruteur(entreprise, session):
        return session.query(Consultant).filter(Consultant.entreprise == entreprise).count()

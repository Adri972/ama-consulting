# AMAConsulting/Offre.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, Float
from .base import Base


class Offre(Base):
    __tablename__ = 'offre'
    type = Column(String, primary_key=True)
    mensualite = Column(Integer)
    #max_feuille = Column(Integer)
    cout_feuille = Column(Float)
    max_consultant = Column(Integer)

    def __repr__(self):
        return "<Offre(type='%s', mensualite='%d', cout_feuille='%.2f', max_consultant='%d')>" % (self.type, self.mensualite, self.cout_feuille, self.max_consultant)

    def ajouter(offres, session):
        for offre in offres:
            session.add(offre)

    def informations_offre(offre, session):
        for instance in session.query(Offre).filter(Offre.type == offre):
            return instance
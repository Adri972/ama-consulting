# AMAConsulting/Transaction.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey
from .base import Base



class Transaction(Base):
    __tablename__ = 'transaction_paiement'
    id_paypal = Column(String, primary_key=True)
    recruteur = Column(String)
    date = Column(String)
    id_facture = Column(Integer)
    montant = Column(Integer)
    status = Column(String)

    def __repr__(self):
        return "<Transaction(id_paypal='%s', recruteur='%s', date='%s', id_facture='%d', montant='%d', status='%s')>" % (self.id_paypal, self.recruteur, self.date, self.id_facture, self.montant, self.status)

    def ajouter(transaction, session ):
        session.add(transaction)
        session.commit()

    def trouver_facture(id_facture, session):
        for instance in session.query(Transaction).filter(Transaction.id_facture == id_facture):
            return instance

    def trouver_recruteur(email, session):
        transactions = [instance for instance in session.query(Transaction).filter(
                        Transaction.recruteur == email)]
        return transactions

    def trouver_date(une_date, session):
        for instance in session.query(Transaction).filter(Transaction.date == une_date):
            return instance



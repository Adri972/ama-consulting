--
-- File generated with SQLiteStudio v3.2.1 on Mon Apr 8 14:33:27 2019
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: abonnement
CREATE TABLE abonnement (
    entreprise TEXT NOT NULL
                    UNIQUE
                    PRIMARY KEY,
    date_debut DATE NOT NULL,
    date_fin   DATE NOT NULL,
    offre      TEXT NOT NULL
                    UNIQUE,
    etat       TEXT CHECK (etat IN ('en_cours', 'en_attente', 'clos') ),
    FOREIGN KEY (
        entreprise
    )
    REFERENCES tenant (entreprise),
    FOREIGN KEY (
        offre
    )
    REFERENCES offre (type) 
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;

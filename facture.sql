--
-- File generated with SQLiteStudio v3.2.1 on Mon Apr 8 14:32:56 2019
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: facture
CREATE TABLE facture (
    id           INTEGER UNIQUE
                         PRIMARY KEY,
    entreprise   TEXT    NOT NULL
                         REFERENCES tenant (entreprise),
    date_facture TEXT    NOT NULL,
    mois         INTEGER NOT NULL,
    annee        INTEGER NOT NULL,
    mensualite   INTEGER NOT NULL,
    nbr_feuille  INTEGER NOT NULL,
    cout_feuille INTEGER NOT NULL,
    sous_total   INTEGER
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;

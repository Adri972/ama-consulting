$(document).ready(function () {
    $("#buttonGo").click(function() {
        $("#buttonGo").addClass("animated");
        $("#buttonGo").addClass("bounceOut");
        setTimeout(function () {
            $("#buttonGo").hide();
            $("#subscribe-card").find(".card-header").removeClass("invisible");
            $("#subscribe-card").find(".card-body").addClass("animated");
            $("#subscribe-card").find(".card-body").addClass("bounceIn");
            $("#subscribe-card").find(".card-body").addClass("delay-4s");
            $("#subscribe-card").find(".card-body").removeClass("invisible");
            $("#subscribe-card").find(".card-header").addClass("animated");
            $("#subscribe-card").find(".card-header").addClass("bounceIn");
            $("#subscribe-card").find(".card-header").addClass("delay-4s");
            $("#texteTab").html("<span class='d-block'>Entrez vos informations</span><span class='text-primary'>personnelle</span>");
        }, 1000);
    });

    $("#btn-next-sub1").click(function() {
        $("#tab1").removeClass("active");
        $("#tab2").addClass("active");
        $("#tab1num").removeClass("active");
        $("#tab1num").addClass("bg-happy-green");
        $("#tab1num").html("<i class='pe-7s-check form-check'></i>");
        $('#tab1').append('<style>#tab1:before{background:#3ac47d;}</style>');
        $("#tab2num").addClass("active");
        $("#card1").removeClass("active");
        $("#card2").addClass("active");
        $("#texteTab").html("<span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Entrez vos informations</font></font><span class='text-primary'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'> de connexion</font></font></span></span>");
    });

    $("#btn-next-sub2").click(function() {
        $("#tab2").removeClass("active");
        $("#tab3").addClass("active");
        $("#tab2num").removeClass("active");
        $("#tab2num").addClass("bg-happy-green");
        $("#tab2num").html("<i class='pe-7s-check form-check'></i>");
        $('#tab2').append('<style>#tab2:before{background:#3ac47d;}</style>');
        $("#tab3num").addClass("active");
        $("#card2").removeClass("active");
        $("#card3").addClass("active");
        $("#texteTab").html("<span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Choisissez</font></font><span class='text-primary'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'> l'abonnement </font></font></span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>pour l'accés à la plateforme</font></font></span>");
    });

    $("#btn-next-sub3").click(function() {
        $("#tab3").removeClass("active");
        $("#tab4").addClass("active");
        $("#tab3num").removeClass("active");
        $("#tab3num").addClass("bg-happy-green");
        $("#tab3num").html("<i class='pe-7s-check form-check'></i>");
        $('#tab3').append('<style>#tab3:before{background:#3ac47d;}</style>');
        $("#tab4num").addClass("active");
        $("#card3").removeClass("active");
        $("#card4").addClass("active");
        $("#texteTab").html("<span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Vérifiez</font></font><span class='text-primary'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'> vos informations</font></font></span></span>");
        load_informations();
    });

    $("#goToPay").click(function() {
        $("#tab4").removeClass("active");
        $("#tab5").addClass("active");
        $("#tab4num").removeClass("active");
        $("#tab4num").addClass("bg-happy-green");
        $("#tab4num").html("<i class='pe-7s-check form-check'></i>");
        $('#tab4').append('<style>#tab4:before{background:#3ac47d;}</style>');
        $("#tab5num").addClass("active");
        $("#card4").removeClass("active");
        $("#card5").addClass("active");
        $("#texteTab").html("<span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Procédez au</font></font><span class='text-primary'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'> paiement</font></font></span></span>");
    });

    $("#btn-prev-sub2").click(function() {
        $("#tab2").removeClass("active");
        $("#tab1").addClass("active");
        $("#tab2num").removeClass("active");
        $("#tab1num").removeClass("bg-happy-green");
        $('#tab1').removeAttr('style','#tab1:before{background:#3ac47d;}');
        $('#tab2').append('<style>#tab2:before{background:#dee2e6;}</style>');
        $("#tab1num").html("<a>1</a>");
        $("#tab1num").addClass("active");
        $("#card2").removeClass("active");
        $("#card1").addClass("active");
        $("#texteTab").html("<span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Entrez vos informations</font></font><span class='text-primary'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'> personnelles</font></font></span></span>");
    });

    $("#btn-prev-sub3").click(function() {
        $("#tab3").removeClass("active");
        $("#tab2").addClass("active");
        $("#tab3num").removeClass("active");
        $("#tab2num").removeClass("bg-happy-green");
        $('#tab2').removeAttr('style','#tab2:before{background:#3ac47d;}');
        $('#tab3').append('<style>#tab3:before{background:#dee2e6;}</style>');
        $("#tab2num").html("<a>2</a>");
        $("#tab2num").addClass("active");
        $("#card3").removeClass("active");
        $("#card2").addClass("active");
        $("#texteTab").html("<span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Entrez vos informations</font></font><span class='text-primary'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'> de connexion</font></font></span></span>");
    });

    $("#btn-prev-sub4").click(function() {
        $("#tab4").removeClass("active");
        $("#tab3").addClass("active");
        $("#tab4num").removeClass("active");
        $("#tab3num").removeClass("bg-happy-green");
        $('#tab3').removeAttr('style','#tab3:before{background:#3ac47d;}');
        $('#tab4').append('<style>#tab4:before{background:#dee2e6;}</style>');
        $("#tab3num").html("<a>3</a>");
        $("#tab3num").addClass("active");
        $("#card4").removeClass("active");
        $("#card3").addClass("active");
        $("#texteTab").html("<span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Choisissez</font></font><span class='text-primary'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'> l'abonnement </font></font></span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>pour l'accés à la plateforme</font></font></span>");
    });

    $("#btn-prev-sub5").click(function() {
        $("#tab5").removeClass("active");
        $("#tab4").addClass("active");
        $("#tab5num").removeClass("active");
        $("#tab4num").removeClass("bg-happy-green");
        $('#tab4').removeAttr('style','#tab4:before{background:#3ac47d;}');
        $("#tab4num").html("<a>4</a>");
        $("#tab4num").addClass("active");
        $("#card5").removeClass("active");
        $("#card4").addClass("active");
        $("#texteTab").html("<span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Vérifiez</font></font><span class='text-primary'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'> vos informations</font></font></span></span>");
    });

    $("#bronze3mois").click(function(){
        update_nombreMois(3);
        remove_active_month('cardSilver');
        remove_active_month('cardGold');
        remove_disabled_button('buttonBronze');
        add_disabled_button('buttonSilver');
        add_disabled_button('buttonGold');
        remove_focus_choice('cardSilver');
        remove_focus_choice('cardGold');
        $("#btn-next-sub3").addClass("d-none");
    });

    $("#bronze6mois").click(function(){
        update_nombreMois(6);
        remove_active_month('cardSilver');
        remove_active_month('cardGold');
        remove_disabled_button('buttonBronze');
        add_disabled_button('buttonSilver');
        add_disabled_button('buttonGold');
        remove_focus_choice('cardSilver');
        remove_focus_choice('cardGold');
        $("#btn-next-sub3").addClass("d-none");
    });

    $("#bronze12mois").click(function(){
        update_nombreMois(12);
        remove_active_month('cardSilver');
        remove_active_month('cardGold');
        remove_disabled_button('buttonBronze');
        add_disabled_button('buttonSilver');
        add_disabled_button('buttonGold');
        remove_focus_choice('cardSilver');
        remove_focus_choice('cardGold');
        $("#btn-next-sub3").addClass("d-none");
    });

    $("#silver3mois").click(function(){
        update_nombreMois(3);
        remove_active_month('cardBronze');
        remove_active_month('cardGold');
        remove_disabled_button('buttonSilver');
        add_disabled_button('buttonBronze');
        add_disabled_button('buttonGold');
        remove_focus_choice('cardBronze');
        remove_focus_choice('cardGold');
        $("#btn-next-sub3").addClass("d-none");
    });

    $("#silver6mois").click(function(){
        update_nombreMois(6);
        remove_active_month('cardBronze');
        remove_active_month('cardGold');
        remove_disabled_button('buttonSilver');
        add_disabled_button('buttonBronze');
        add_disabled_button('buttonGold');
        remove_focus_choice('cardBronze');
        remove_focus_choice('cardGold');
        $("#btn-next-sub3").addClass("d-none");
    });

    $("#silver12mois").click(function(){
        update_nombreMois(12);
        remove_active_month('cardBronze');
        remove_active_month('cardGold');
        remove_disabled_button('buttonSilver');
        add_disabled_button('buttonBronze');
        add_disabled_button('buttonGold');
        remove_focus_choice('cardBronze');
        remove_focus_choice('cardGold');
        $("#btn-next-sub3").addClass("d-none");
    });

    $("#gold3mois").click(function(){
        update_nombreMois(3);
        remove_active_month('cardBronze');
        remove_active_month('cardSilver');
        remove_disabled_button('buttonGold');
        add_disabled_button('buttonBronze');
        add_disabled_button('buttonSilver');
        remove_focus_choice('cardSilver');
        remove_focus_choice('cardBronze');
        $("#btn-next-sub3").addClass("d-none");
    });       

    $("#gold6mois").click(function(){
        update_nombreMois(6);
        remove_active_month('cardBronze');
        remove_active_month('cardSilver');
        remove_disabled_button('buttonGold');
        add_disabled_button('buttonBronze');
        add_disabled_button('buttonSilver');
        remove_focus_choice('cardSilver');
        remove_focus_choice('cardBronze');
        $("#btn-next-sub3").addClass("d-none");
    });       

    $("#gold12mois").click(function(){
        update_nombreMois(12);
        remove_active_month('cardBronze');
        remove_active_month('cardSilver');
        remove_disabled_button('buttonGold');
        add_disabled_button('buttonBronze');
        add_disabled_button('buttonSilver');
        remove_focus_choice('cardSilver');
        remove_focus_choice('cardBronze');
        $("#btn-next-sub3").addClass("d-none");
    });

    $("#buttonBronze").click(function(e){
        if (! $(this).hasClass("disabled")){
            update_forfait('bronze', 10);
            add_focus_choice('cardBronze');
            remove_focus_choice('cardSilver');
            remove_focus_choice('cardGold');
            add_button_active_choice('buttonBronze');
            remove_button_active_choice('buttonSilver');
            remove_button_active_choice('buttonGold');
            $("#btn-next-sub3").removeClass("d-none");
        }
    });

    $("#buttonSilver").click(function(e){
        if (! $(this).hasClass("disabled")){
            update_forfait('silver', 20);
            add_focus_choice('cardSilver');
            remove_focus_choice('cardBronze');
            remove_focus_choice('cardGold');
            add_button_active_choice('buttonSilver');
            remove_button_active_choice('buttonBronze');
            remove_button_active_choice('buttonGold');
            $("#btn-next-sub3").removeClass("d-none");
        }            
    });

    $("#buttonGold").click(function(e){
        if (! $(this).hasClass("disabled")){
            update_forfait('gold', 35);
            add_focus_choice('cardGold');
            remove_focus_choice('cardSilver');
            remove_focus_choice('cardBronze');
            add_button_active_choice('buttonGold');
            remove_button_active_choice('buttonSilver');
            remove_button_active_choice('buttonBronze');
            $("#btn-next-sub3").removeClass("d-none");
        }            
    });
});

var nombreMois = 0;
var forfait = "";
var prix = 0;

function validation_form_tab1(){
    var nom = document.getElementById("inp-nom").value;
    var prenom = document.getElementById("inp-prenom").value;
    var tel = document.getElementById("inp-tel").value;
    var entreprise = document.getElementById("inp-entreprise").value;

    if(nom != "" && prenom != "" && tel != "" && entreprise != ""){
        document.getElementById("btn-next-sub1").classList.remove("d-none");
    } else {
        document.getElementById("btn-next-sub1").classList.add("d-none");
    }
}

function validation_form_tab2(){
    var email = document.getElementById("inp-email").value;
    var password = document.getElementById("password").value;
    var repeat_password = document.getElementById("repeatPassword").value;

    if(email != "" && password != "" && repeat_password != "" && checkEmail() && checkPasswords()){
        document.getElementById("btn-next-sub2").classList.remove("d-none");
    } else {
        document.getElementById("btn-next-sub2").classList.add("d-none");
    }
}

function checkPasswords(){
    var password = document.getElementById("password");
    var repeat_password = document.getElementById("repeatPassword");   
    var invalid_password = document.getElementById("invalid-password");

    if(password.value != repeat_password.value && password.value != "" && repeat_password.value != ""){
        repeat_password.setAttribute("style", "border-color: #d92550 !important");
        invalid_password.setAttribute("style", "display: inherit;");

        return false;

    } else if(repeat_password.value != ""){
        repeat_password.setAttribute("style", "border-color: #3ac47d !important");
        password.setAttribute("style", "border-color: #3ac47d !important");
        invalid_password.setAttribute("style", "display: none;");

        return true;
    }
}

function checkEmail(){
    var email = document.getElementById("inp-email");
    var invalid_email = document.getElementById("invalid-email");

    if((email.value).includes("@")){
        email.setAttribute("style", "border-color: #3ac47d !important");
        invalid_email.setAttribute("style", "display: none;");;
        return true;
    } else{
        email.setAttribute("style", "border-color: #d92550 !important");
        invalid_email.setAttribute("style", "display: inherit;");

        return false;
    }
}
function load_informations(){
    var r_nom = document.getElementById("resumeNom");
    var r_prenom = document.getElementById("resumePrenom");
    var r_tel = document.getElementById("resumeTel");
    var r_entreprise = document.getElementById("resumeEntreprise");
    var r_email = document.getElementById("resumeEmail");
    var r_password = document.getElementById("resumePassword");
    var r_nombreMois = document.getElementById("resumeNombreMois");
    var r_forfait = document.getElementById("resumeForfait");
    var r_prix = document.getElementById("resumePrix");
    var r_nombreMois2 = document.getElementById("resumeNombreMois2");
    var r_forfait2 = document.getElementById("resumeForfait2");
    var r_prix2 = document.getElementById("resumePrix2");

    var nom = document.getElementById("inp-nom").value;
    var prenom = document.getElementById("inp-prenom").value;
    var tel = document.getElementById("inp-tel").value;
    var entreprise = document.getElementById("inp-entreprise").value;
    var email = document.getElementById("inp-email").value;
    var password = document.getElementById("password").value;

    var asterix = "*";
    var hidden_password = asterix.repeat(password.length);
    r_nom.innerHTML = nom;
    r_prenom.innerHTML = prenom;
    r_tel.innerHTML = tel;
    r_entreprise.innerHTML = entreprise;
    r_email.innerHTML = email;
    r_password.innerHTML = hidden_password;

    r_nombreMois.innerHTML = " " + nombreMois.toString() + " mois";
    r_prix.innerHTML = " " + prix.toString() + ".00 $";

    if(forfait == "bronze"){
        r_forfait.innerHTML = "<img class='img-plan-resume'src='../static/images/bronze.png') }}'/>";
    } else if(forfait == "silver"){
        r_forfait.innerHTML = "<img class='img-plan-resume'src='../static/images/silver.png') }}'/>";
    } else {
        r_forfait.innerHTML = "<img class='img-plan-resume'src='../static/images/gold.png') }}'/>";       
    }
    r_nombreMois2.innerHTML = " " + nombreMois.toString() + " mois";
    r_prix2.innerHTML = " " + prix.toString() + ".00 $";
    r_forfait2.innerHTML = "<input name='forfait' type='text' id='inp-forfait' readonly value='" + forfait + "'/>";

    if (document.getElementById("paypal-button-container").childElementCount == 0){
        paypal.Buttons({
            createOrder: function(data, actions) {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: prix
                        }
                    }]
                });
            },
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    var transaction_id = details.id;
                    var transaction_date = details.create_time;
                    var transaction_price = details.purchase_units[0].amount.value;
                    var transaction_status = details.status;
                    var recruteur = document.getElementById("inp-email").value;

                    // save transaction info
                    var date_facture = transaction_date.substr(0,10);
                    var mois = parseInt(date_facture.substr(5,7)) - 1;
                    var annee = parseInt(date_facture.substr(0,4));

                    payBill(entreprise, date_facture, 0, annee, transaction_price, 0, 0, transaction_price, "non payee", "Nouvelle Inscription");

                    setTimeout(function(){
                        addTransaction(recruteur, date_facture, transaction_price, transaction_id, transaction_status);
                    }, 100);

                    setTimeout(function(){
                        document.getElementById("subscribeForm").submit();
                    }, 200);
                });
            },
            onCancel: function (data) {

            },

            style: {
                layout:  'horizontal',
                color:   'blue',
                shape:   'pill',
                label:   'pay',
                tagline: 'false'
            }
        }).render('#paypal-button-container');
    }
}

function update_nombreMois(nbMois){
    nombreMois = nbMois;
}

function update_forfait(nom, f_prix){
    forfait = nom;
    prix = f_prix;
}

function remove_active_month(card_id){
    var card = document.getElementById(card_id);
    var months = card.getElementsByClassName("btn-group")[0];

    for(var i=0 ; i < 3; i++){
        months.getElementsByTagName("label")[i].classList.remove("active");
    }
}

function remove_button_active_choice(button_id){
    var button = document.getElementById(button_id);
    button.classList.remove("active");
}

function add_button_active_choice(button_id){
    var button = document.getElementById(button_id);
    button.classList.add("active");
}

function add_focus_choice(id){
    var card = document.getElementById(id);
    card.classList.add("card-shadow-primary");
    card.classList.add("border");
    card.classList.add("border-primary");
}

function remove_focus_choice(id){
    var card = document.getElementById(id);
    card.classList.remove("card-shadow-primary");
    card.classList.remove("border");
    card.classList.remove("border-primary");
}

function remove_disabled_button(button_id){
    var button = document.getElementById(button_id);
    button.classList.remove("disabled");
    button.removeAttribute("data-toggle");
    button.removeAttribute("data-placement");
    button.removeAttribute("data-original-title");
}

function add_disabled_button(button_id){
    var button = document.getElementById(button_id);
    button.classList.add("disabled");
    button.setAttribute("data-toggle", "tooltip");
    button.setAttribute("data-placement", "bottom");
    button.setAttribute("data-original-title", "Sélectionnez une mensualité !");
}

$(document).ready(function () {
    $("#btn-next-sub1").click(function() {
        $("#tab1").removeClass("active");
        $("#tab2").addClass("active");
        $("#tab1num").removeClass("active");
        $("#tab1num").addClass("bg-happy-green");
        $("#tab1num").html("<i class='pe-7s-check form-check'></i>");
        $('#tab1').append('<style>#tab1:before{background:#3ac47d;}</style>');
        $("#tab2num").addClass("active");
        $("#card1").removeClass("active");
        $("#card2").addClass("active");
        load_informations();
    });
    
    $("#btn-prev-sub2").click(function() {
        $("#tab2").removeClass("active");
        $("#tab1").addClass("active");
        $("#tab2num").removeClass("active");
        $("#tab1num").removeClass("bg-happy-green");
        $('#tab1').removeAttr('style','#tab1:before{background:#3ac47d;}');
        $('#tab2').append('<style>#tab2:before{background:#dee2e6;}</style>');
        $("#tab1num").html("<a>1</a>");
        $("#tab1num").addClass("active");
        $("#card2").removeClass("active");
        $("#card1").addClass("active");
        $("#texteTab").html("<span><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'>Procédez au</font></font><span class='text-primary'><font style='vertical-align: inherit;'><font style='vertical-align: inherit;'> paiement</font></font></span></span>");
    });
    
    
    $("#bronze3mois").click(function(){
        update_nombreMois(3);
        remove_disabled_button('buttonBronze');
        $("#btn-next-sub1").addClass("d-none");
    });

    $("#bronze6mois").click(function(){
        update_nombreMois(6);
        remove_disabled_button('buttonBronze');
        $("#btn-next-sub1").addClass("d-none");
    });

    $("#bronze12mois").click(function(){
        update_nombreMois(12);
        remove_disabled_button('buttonBronze');
        $("#btn-next-sub1").addClass("d-none");
    });

    $("#silver3mois").click(function(){
        update_nombreMois(3);
        remove_disabled_button('buttonSilver');
        $("#btn-next-sub1").addClass("d-none");
    });

    $("#silver6mois").click(function(){
        update_nombreMois(6);
        remove_disabled_button('buttonSilver');
        $("#btn-next-sub1").addClass("d-none");
    });

    $("#silver12mois").click(function(){
        update_nombreMois(12);
        remove_disabled_button('buttonSilver');
        $("#btn-next-sub1").addClass("d-none");
    });

    $("#gold3mois").click(function(){
        update_nombreMois(3);
        remove_disabled_button('buttonGold');
        $("#btn-next-sub1").addClass("d-none");
    });       

    $("#gold6mois").click(function(){
        update_nombreMois(6);
        remove_disabled_button('buttonGold');
        $("#btn-next-sub1").addClass("d-none");
    });       

    $("#gold12mois").click(function(){
        update_nombreMois(12);
        remove_disabled_button('buttonGold');
        $("#btn-next-sub1").addClass("d-none");
    });
    
    
    $("#buttonBronze").click(function(e){
        if (! $(this).hasClass("disabled")){
            update_forfait('bronze', 10);
            add_focus_choice('cardBronze');
            add_button_active_choice('buttonBronze');
            $("#btn-next-sub1").removeClass("d-none");
        }
    });

    $("#buttonSilver").click(function(e){
        if (! $(this).hasClass("disabled")){
            update_forfait('silver', 20);
            add_focus_choice('cardSilver');
            add_button_active_choice('buttonSilver');
            $("#btn-next-sub1").removeClass("d-none");
        }            
    });

    $("#buttonGold").click(function(e){
        if (! $(this).hasClass("disabled")){
            update_forfait('gold', 35);
            add_focus_choice('cardGold');
            add_button_active_choice('buttonGold');
            $("#btn-next-sub1").removeClass("d-none");
        }            
    });
});

var nombreMois = 0;
var forfait = "";
var prix = 0;

function update_nombreMois(nbMois){
    nombreMois = nbMois;
}

function update_forfait(nom, f_prix){
    forfait = nom;
    prix = f_prix;
}

function add_button_active_choice(button_id){
    var button = document.getElementById(button_id);
    button.classList.add("active");
}

function add_focus_choice(id){
    var card = document.getElementById(id);
    card.classList.add("card-shadow-primary");
    card.classList.add("border");
    card.classList.add("border-primary");
}

function remove_disabled_button(button_id){
    var button = document.getElementById(button_id);
    button.classList.remove("disabled");
    button.removeAttribute("data-toggle");
    button.removeAttribute("data-placement");
    button.removeAttribute("data-original-title");
}

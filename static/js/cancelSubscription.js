$(document).ready(function () {
    $("#btn-next-sub1").click(function() {
        $("#tab1").removeClass("active");
        $("#tab2").addClass("active");
        $("#tab1num").removeClass("active");
        $("#tab1num").addClass("bg-happy-green");
        $("#tab1num").html("<i class='pe-7s-check form-check'></i>");
        $('#tab1').append('<style>#tab1:before{background:#3ac47d;}</style>');
        $("#tab2num").addClass("active");
        $("#card1").removeClass("active");
        $("#card2").addClass("active");
        load_informations();
    });
    
    $("#btn-next-sub2").click(function() {
        $("#tab2").removeClass("active");
        $("#tab3").addClass("active");
        $("#tab2num").removeClass("active");
        $("#tab2num").addClass("bg-happy-green");
        $("#tab2num").html("<i class='pe-7s-check form-check'></i>");
        $('#tab2').append('<style>#tab2:before{background:#3ac47d;}</style>');
        $("#tab3num").addClass("active");
        $("#card2").removeClass("active");
        $("#card3").addClass("active");
    });

    $("#btn-prev-sub2").click(function() {
        $("#tab2").removeClass("active");
        $("#tab1").addClass("active");
        $("#tab2num").removeClass("active");
        $("#tab1num").removeClass("bg-happy-green");
        $('#tab1').removeAttr('style','#tab1:before{background:#3ac47d;}');
        $('#tab2').append('<style>#tab2:before{background:#dee2e6;}</style>');
        $("#tab1num").html("<a>1</a>");
        $("#tab1num").addClass("active");
        $("#card2").removeClass("active");
        $("#card1").addClass("active");
    });
});

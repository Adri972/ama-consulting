function updateTimeSheetDayAMS(id) {
    var heure = document.getElementById("amS_" + id).value;
    var id_info = id.split('_');
    var ordre_achat = id_info[0];
    var date_str = id_info[1];
    var save = document.getElementById("saveHour");

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                save.classList.remove("d-none");
                save.classList.add("d-initial");
                save.innerHTML = xhr.responseText;
                setTimeout(function(){ 
                    save.classList.remove("d-initial");
                    save.classList.add("d-none");
                }, 1500);


            } else {
                window.location.href = "/erreur";
            }
        }
    };

    xhr.open("GET", "/saveTimeSheet/" + ordre_achat + "/" + date_str + "/jour_" + id_info[3] + "/" + heure + "/AMS",true);
    xhr.send();
}

function updateTimeSheetDayAME(id) {
    var heure = document.getElementById("amE_" + id).value;
    var id_info = id.split('_');
    var ordre_achat = id_info[0];
    var date_str = id_info[1];
    var save = document.getElementById("saveHour");

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                save.classList.remove("d-none");
                save.classList.add("d-initial");
                save.innerHTML = xhr.responseText;
                setTimeout(function(){ 
                    save.classList.remove("d-initial");
                    save.classList.add("d-none");
                }, 1500);


            } else {
                window.location.href = "/erreur";
            }
        }
    };

    xhr.open("GET", "/saveTimeSheet/" + ordre_achat + "/" + date_str + "/jour_" + id_info[3] + "/" + heure + "/AME",true);
    xhr.send();
}

function updateTimeSheetDayPMS(id) {
    var heure = document.getElementById("pmS_" + id).value;
    var id_info = id.split('_');
    var ordre_achat = id_info[0];
    var date_str = id_info[1];
    var save = document.getElementById("saveHour");

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                save.classList.remove("d-none");
                save.classList.add("d-initial");
                save.innerHTML = xhr.responseText;
                setTimeout(function(){ 
                    save.classList.remove("d-initial");
                    save.classList.add("d-none");
                }, 1500);


            } else {
                window.location.href = "/erreur";
            }
        }
    };

    xhr.open("GET", "/saveTimeSheet/" + ordre_achat + "/" + date_str + "/jour_" + id_info[3] + "/" + heure + "/PMS",true);
    xhr.send();
}

function updateTimeSheetDayPME(id) {
    var heure = document.getElementById("pmE_" + id).value;
    var id_info = id.split('_');
    var ordre_achat = id_info[0];
    var date_str = id_info[1];
    var save = document.getElementById("saveHour");

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                save.classList.remove("d-none");
                save.classList.add("d-initial");
                save.innerHTML = xhr.responseText;
                setTimeout(function(){ 
                    save.classList.remove("d-initial");
                    save.classList.add("d-none");
                }, 1500);


            } else {
                window.location.href = "/erreur";
            }
        }
    };

    xhr.open("GET", "/saveTimeSheet/" + ordre_achat + "/" + date_str + "/jour_" + id_info[3] + "/" + heure + "/PME",true);
    xhr.send();
}

function getNowTimeSheet(consultant){
    var list = document.getElementById("timesheets_" + consultant);

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                list.innerHTML = xhr.responseText;
            } else {
                window.location.href = "/erreur";
            }
        }
    };

    xhr.open("GET", "/listTimeSheetsNow/" + consultant,true);
    xhr.send();
}

function getAllTimeSheets(consultant){
    var list = document.getElementById("timesheets_" + consultant);

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                list.innerHTML = xhr.responseText;
            } else {
                window.location.href = "/erreur";
            }
        }
    };

    xhr.open("GET", "/listTimeSheetsAll/" + consultant,true);
    xhr.send();
}

function getSubmittedTimeSheets(consultant){
    var list = document.getElementById("timesheets_" + consultant);

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                list.innerHTML = xhr.responseText;
            } else {
                window.location.href = "/erreur";
            }
        }
    };

    xhr.open("GET", "/listTimeSheetsSubmitted/" + consultant,true);
    xhr.send();
}

function getPendingTimeSheets(consultant){
    var list = document.getElementById("timesheets_" + consultant);

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                list.innerHTML = xhr.responseText;
            } else {
                window.location.href = "/erreur";
            }
        }
    };

    xhr.open("GET", "/listTimeSheetsPending/" + consultant,true);
    xhr.send();
}

function readNotification(notif_id, line){
    var header = document.getElementById(line);
    var loop_index = line.substr(line.length - 1, line.length);
    var read_icon = document.getElementById("readIcon_" + loop_index);
    var xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                header.style.backgroundColor = "initial";
                read_icon.classList.remove("fas");
                read_icon.classList.add("far");
            }
        }
    };
    xhr.open("GET", "/readNotification/" + notif_id,true);
    xhr.send();
}

function addTransaction(recruteur, date, montant, id_paypal, status){
    var xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                return true;
            }
        }
    };
    
    xhr.open("GET", "/addTransaction/" + recruteur + "/" + date + "/" + montant + "/" + id_paypal + "/" + status, true);
    xhr.send();
}

function payBill(entreprise, date_facture, mois, annee, mensualite, nbr_feuille, cout_feuille, sous_total, etat, details){
    var xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                return true;
            }
        }
    };
    xhr.open("GET", "/payBill/" + entreprise + "/" + date_facture + "/" + mois + "/" + annee + "/" + mensualite + "/" + nbr_feuille + "/" + cout_feuille + "/" + sous_total + "/" + etat + "/" + details, true);
    xhr.send();
}

function modifySubscription(entreprise, date_debut, offre, mensualite){
    var xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            document.getElementsByTagName("body")[0].innerHTML = xhr.responseText;
        }
    };
    xhr.open("GET", "/modifySubscription_submit/" + entreprise + "/" + date_debut + "/" + offre + "/" + mensualite, true);
    xhr.send();
}

function renewSubscription(entreprise, date_debut, mensualite){
    var xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            document.getElementsByTagName("body")[0].innerHTML = xhr.responseText;
        }
    };
    xhr.open("GET", "/renewSubscription_submit/" + entreprise + "/" + date_debut + "/" + mensualite, true);
    xhr.send();
}

function cancelSubscription(){
    var xhr = new XMLHttpRequest();
    var password = document.getElementById("password").value;
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                document.getElementsByTagName("body")[0].innerHTML = xhr.responseText;
            } else {
                document.getElementById("infosPassword").classList.remove("d-none");
                document.getElementById("infosPassword").innerHTML = xhr.responseText;
            }
        }
    };
    xhr.open("POST", "/cancelSubscription_submit", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("password=" + password);
}

function payAllBill(recruteur, entreprise, date, montant, id_paypal, status){
    var xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                return true;
            }
        }
    };
    
    xhr.open("GET", "/payAllBill/" + recruteur + "/" + entreprise + "/" + date + "/" + montant + "/" + id_paypal + "/" + status, true);
    xhr.send();
}
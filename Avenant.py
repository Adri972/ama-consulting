# AMAConsulting/Avenant.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from .base import Base

class Avenant(Base):
    __tablename__ = 'avenant'
    date_avenant = Column(String)
    date_fin = Column(String)
    ordre_achat = Column(Integer, ForeignKey('ordre_achat.id'), primary_key=True)
    contact = Column(String, ForeignKey('contact.email'))

    def __repr__(self):
        return "<Avenant(date_avenant='%s', date_fin='%s', ordre_achat='%d', contact='%s')>" \
        % (self.date_avenant, self.date_fin, self.ordre_achat, self.contact)

    def lister(session):
        return [instance for instance in session.query(Avenant)]
    
    def ajouter_multi(avenants, session):
        for avenant in avenants:
            session.add(avenant)

    def ajouter(avenant, session):
        session.add(avenant)
        session.commit()

    def afficher_avenant(contact, session):
        for instance in session.query(Avenant).filter(Avenant.contact.is_(session.query(Avenant.contact).filter(Avenant.contact == contact))):
            return instance

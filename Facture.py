# AMAConsulting/Facture.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey, Float
from .base import Base


class Facture(Base):
    __tablename__ = 'facture'
    id = Column(Integer, primary_key=True)
    entreprise = Column(String)
    date_facture = Column(String)
    mois = Column(Integer)
    annee = Column(Integer)
    mensualite = Column(Integer)
    nbr_feuille = Column(Integer)
    cout_feuille = Column(Float)
    sous_total = Column(Float)
    etat = Column(String)
    details = Column(String)

    def __repr__(self):
        return "<Facture(id='%d', entreprise='%s', date_facture='%s', mois='%d', annee='%d', mensualite='%d', nbr_feuille='%d', cout_feuille='%.2f, sous_total='%.2f', etat='%s', details='%s')>" % (self.id, self.entreprise, self.date_facture, self.mois, self.annee, self.mensualite, self.nbr_feuille, self.cout_feuille, self.sous_total, self.etat, self.details)


    def ajouter(facture, session):
        session.add(facture)
        session.commit()

    def trouver_tenant(tenant, session):
        factures = [instance for instance in session.query(Facture).filter(Facture.entreprise == tenant)]
        return factures

    def trouver_id_par_date_montant(date_facture, montant, session):
        for instance in session.query(Facture.id).filter(Facture.date_facture == date_facture).filter(Facture.sous_total == montant):
            return instance
        
    def trouver_par_id_tenant(id_facture, entreprise, session):
        for instance in session.query(Facture).filter(Facture.id == id_facture).filter(Facture.entreprise == entreprise):
            return instance
        
    def update_status(id_facture, session):
        for instance in session.query(Facture).filter(Facture.id == id_facture):
            instance.etat = "payee"
            session.commit()
    
    def trouver_non_payee(entreprise, session):
        factures = [instance for instance in session.query(Facture).filter(Facture.entreprise == entreprise).filter(Facture.etat == 'non payee')]
        return factures
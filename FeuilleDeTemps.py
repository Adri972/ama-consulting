# AMAConsulting/FeuilleDeTemps.py
from sqlalchemy import Column, String, Integer, ForeignKey
from .base import Base
from .Ordre_achat import Ordre_achat
from datetime import datetime, date


class FeuilleDeTemps(Base):
    __tablename__ = 'feuille_temps'
    id = Column(Integer, primary_key=True)
    ordre_achat_id = Column(Integer, ForeignKey('ordre_achat.id'))
    date_debut = Column(String)
    date_fin = Column(String)
    jour_1 = Column(String)
    jour_2 = Column(String)
    jour_3 = Column(String)
    jour_4 = Column(String)
    jour_5 = Column(String)
    jour_6 = Column(String)
    jour_7 = Column(String)
    etat = Column(String)
    approbateur = Column(String)

    def __repr__(self):
        return "<FeuilleDeTemps(id='%d', ordre_achat_id='%d', date_debut='%s', date_fin='%s', jour_1='%s', jour_2='%s', jour_3='%s', jour_4='%s', jour_5='%s', jour_6='%s', jour_7='%s'), etat='%s'>" \
        % (self.id, self.ordre_achat_id, self.date_debut, self.date_fin, self.jour_1, self.jour_2, self.jour_3, self.jour_4, self.jour_5, self.jour_6, self.jour_7, self.etat)

    @staticmethod
    def ajouter(feuille_temp, session):
        session.add(feuille_temp)

    def afficher(ordre_achat, date_debut, session):
        for instance in session.query(FeuilleDeTemps).filter(FeuilleDeTemps.ordre_achat_id == ordre_achat).filter(FeuilleDeTemps.date_debut == date_debut):
            return instance

    def afficher_par_status(ordre_achat, etat, session):
        timesheets = [instance for instance in session.query(FeuilleDeTemps).filter(FeuilleDeTemps.ordre_achat_id == ordre_achat).filter(FeuilleDeTemps.etat == etat)]
        return timesheets
    
    def afficher_derniere_soumise(ordre_achat, date_debut, session):
        timesheets = [instance for instance in session.query(FeuilleDeTemps).filter(FeuilleDeTemps.ordre_achat_id == ordre_achat).filter(FeuilleDeTemps.date_debut == date_debut).filter(FeuilleDeTemps.etat == "soumise")]
        return timesheets
        
    def lister_mois(ordre_achat, date_debut, session ):
        #for instance in session.query(FeuilleDeTemps).filter(FeuilleDeTemps.ordre_achat_id == ordre_achat).filter(FeuilleDeTemps.date_debut.like('%{}%'.format(date_debut[0:7]))):
        #    print(instance)
        return session.query(FeuilleDeTemps).filter(FeuilleDeTemps.ordre_achat_id == ordre_achat).filter(
            FeuilleDeTemps.date_debut.like('%{}%'.format(date_debut[0:7]))).count()

    def mois_courant(ordre_achat, session ):
        today = datetime.today()
        ce_mois = str(date(today.year, today.month, 1))

        return session.query(FeuilleDeTemps).filter(FeuilleDeTemps.ordre_achat_id == ordre_achat).filter(
            FeuilleDeTemps.date_debut.like('%{}%'.format(ce_mois[0:7]))).count()    

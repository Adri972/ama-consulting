# AMAConsulting/Soumission.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from .base import Base


class Soumission(Base):
    __tablename__ = 'soumission'
    id = Column(Integer, primary_key=True)
    date_soumission = Column(String)
    recruteur = Column(String, ForeignKey('recruteur.email'))
    client = Column(String, ForeignKey('client.nom_client'))
    consultant = Column(String, ForeignKey('consultant.email'))
    demande_id = Column(Integer, ForeignKey('demande.id'))
    taux_horaire = Column(Integer)
    duree = Column(String)
    details = Column(String)

    def __repr__(self):
        return "<Soumission( id='%s', date_soumission='%s', recruteur='%s', client='%s', consultant='%s', demande_id='%d', taux_horaire='%d', duree='%s', details='%s')>" \
        % (self.id, self.date_soumission, self.recruteur, self.client, self.consultant, self.demande_id, self.taux_horaire, self.duree, self.details )

    def afficher_id(soumission_id, session):
        for instance in session.query(Soumission).filter(Soumission.id == soumission_id):
            return instance
    
    def afficher_client(soumission, session):
        for instance in session.query(Soumission.client).filter(Soumission.id == soumission):
            return instance

    def afficher_consultant(soumission, session):
        for instance in session.query(Soumission.consultant).filter(Soumission.id == soumission):
            return instance

    def ajouter_multi(soumissions, session):
        for soumission in soumissions:
            session.add(soumission)
            
    def afficher_soumission_id_par_client(client, session):
        list_id = [instance for instance in session.query(Soumission.id).filter(Soumission.client == client)]
        return list_id

    def ajouter(soumission, session):
        session.add(soumission)
        session.commit()

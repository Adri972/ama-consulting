    # AMAConsulting/Client.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey
from .base import Base
from .Contact import Contact
from .Demande import Demande
from .Soumission import Soumission
from .Ordre_achat import Ordre_achat
from .Dotation import Dotation
from .Avenant import Avenant


class Client(Base):
    __tablename__ = 'client'
    nom_client = Column(String, primary_key=True)
    entreprise = Column(String, ForeignKey('tenant.entreprise'))
    telephone = Column(String)
    adresse = Column(String)

    def __repr__(self):
        return "<Client(nom_client='%s', entreprise='%s', telephone='%s', adresse='%s')>" \
        % (self.nom_client, self.entreprise, self.telephone, self.adresse)

    def afficher_contact(client, session):
        for instance in session.query(Contact).filter(
            Contact.client.is_(session.query(Contact.client).filter(Contact.client == client))):
            return instance

    def afficher_gestionnaire(client, session):
        for instance in session.query(Contact).filter(
            Contact.client.is_(session.query(Contact.client).filter(Contact.client == client))).filter(Contact.fonction == 'gestionnaire'):
            return instance

    def afficher_demande(client, session):
        for instance in session.query(Demande).filter(Demande.client == client):
            return instance

    def afficher_soumission(client, session):
        for instance in session.query(Soumission).filter(Soumission.client == client):
            return instance

    def afficher_oa(client, session):
        for instance in session.query(Ordre_achat).filter(
            Ordre_achat.soumission_id.is_(session.query(Soumission.id).filter(Soumission.client == client))):
            return instance

    def afficher_dotation(client, session):
        for instance in session.query(Dotation).filter( 
            Dotation.ordre_achat.is_(session.query(Soumission.id).filter(Soumission.client == client))):
            return instance

    def afficher_avenant(client, session):
        for instance in session.query(Avenant).filter(
            Avenant.ordre_achat.is_(session.query(Soumission.id).filter(Soumission.client == client))):
            return instance

    def afficher_tenant(client, session):
        for instance in session.query(Client.entreprise).filter(Client.nom_client == client):
            return instance

    @staticmethod
    def ajouter_multi(clients, session):
        for client in clients:
            session.add(client)

    def ajouter(client, session):
        session.add(client)
        session.commit()

    def gestion_consultants(client, session):
        consultants = [instance for instance in session.query(
            Soumission.consultant).filter(Soumission.client == client)]
        return consultants

    def afficher_client(client, session):
        client = [instance for instance in session.query(
            Client).filter(Client.nom_client == client).all()]
        return client

    def get_total_client_par_recruteur(entreprise, session):
        return session.query(Client).filter(Client.entreprise == entreprise).count()

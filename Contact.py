# AMAConsulting/Contact.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey
from .base import Base
from .Demande import Demande
from .Soumission import Soumission

class Contact(Base):
    __tablename__ = 'contact'
    id = Column(Integer, primary_key=True)
    nom = Column(String)
    prenom = Column(String)
    email = Column(String)
    telephone = Column(String)
    fonction =  Column(String)
    salt = Column(String)
    hash =  Column(String)
    client = Column(String, ForeignKey('client.nom_client'))
    entreprise = Column(String, ForeignKey('tenant.entreprise'))

    def __repr__(self):
        return "<Contact( prenom='%s', nom='%s', email='%s', telephone='%s', fonction='%s', client='%s',entreprise='%s')>" \
        % (self.nom, self.prenom, self.email, self.telephone, self.fonction, self.client, self.entreprise)

    @staticmethod
    def ajouter(contacts, session):
        for contact in contacts:
            session.add(contact)

    @staticmethod
    def afficher_par_email(email, session):
        for instance in session.query(Contact).filter(Contact.email == email):
            print(instance)

    @staticmethod
    def afficher_par_tenant(tenant, session):
        for instance in session.query(Contact).filter(Contact.entreprise == tenant):
            print(instance)

    @staticmethod
    def afficher_par_telephone(telephone, session):
        for instance in session.query(Contact).filter(Contact.telephone == telephone):
            print(instance)

    @staticmethod
    def afficher_soumission_contact(email, session):
        for instance in session.query(Soumission).filter(Soumission.demande_id.is_(session.query(Demande.id).filter(Demande.contact == email))):
            print(instance)
    
    def gestion_contacts(client, session):
        contacts = [instance for instance in session.query(
            Contact).filter(Contact.client == client)]
        return contacts

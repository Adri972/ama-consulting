# AMAConsulting/database.py

import sqlite3
import configparser
import io


def recruiteur_display(row):
    return {"nom": row[0], "prenom": row[1], "email": row[2], "tenant": row[4],
            "photo": row[5]}


def consultant_display(row):
    return {"nom": row[1], "prenom": row[2], "email": row[3],
            "tenant": row[12], "photo": row[0]}


def client_display(row):
    return {"nom": row[1], "prenom": row[2], "email": row[3],
            "tenant": row[7], "photo": row[0]}


class Database:
    def __init__(self):
        self.connection = None

    def get_connection(self):
        if self.connection is None:
            self.connection = sqlite3.connect('db/ama.db')
        return self.connection

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()

    def save_session(self, id_session, username):
        connection = self.get_connection()
        connection.execute(("INSERT INTO sessions(id_session, utilisateur) "
                            "VALUES(?, ?)"), (id_session, username))
        connection.commit()

    def delete_session(self, id_session):
        connection = self.get_connection()
        connection.execute(("DELETE FROM sessions where id_session=?"),
                           (id_session,))
        connection.commit()

    def get_session(self, id_session):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT utilisateur FROM sessions WHERE id_session=?"),
                       (id_session,))

        data = cursor.fetchone()

        if data is None:
            return None
        else:
            return data[0]

    def insert_new_member(self, nom, prenom, email, telephone, entreprise,
                          salt, hashed_password):

        connection = self.get_connection()

        connection.execute(("INSERT INTO recruteur(nom, prenom, email, "
                            "telephone, entreprise, salt, hash) "
                            "VALUES(?, ?, ?, ?, ?, ?, ?)"),
                           (nom, prenom, email, telephone, entreprise,  salt,
                            hashed_password))

        connection.commit()
        

    def get_user_login_info(self, username):
        cursor1 = self.get_connection().cursor()
        cursor2 = self.get_connection().cursor()
        cursor3 = self.get_connection().cursor()

        cursor1.execute(("SELECT salt, hash FROM recruteur WHERE email=?"),
                        (username,))
        cursor2.execute(("SELECT salt, hash FROM consultant WHERE email=?"),
                        (username,))
        cursor3.execute(("SELECT salt, hash FROM contact WHERE email=?"),
                        (username,))

        recruteur = cursor1.fetchone()
        consultant = cursor2.fetchone()
        client = cursor3.fetchone()

        if recruteur is not None:
            return recruteur[0], recruteur[1]
        elif consultant is not None:
            return consultant[0], consultant[1]
        elif client is not None:
            return client[0], client[1]
        else:
            return None

    def get_user_exists(self, username):
        cursor = self.get_connection().cursor()
        cursor.execute(("SELECT * FROM recruteur WHERE email=?"), (username,))

        exists = cursor.fetchone()

        if exists is None:
            return None
        else:
            return exists

    def get_user_all_infos(self, username):
        cursor1 = self.get_connection().cursor()
        cursor2 = self.get_connection().cursor()
        cursor3 = self.get_connection().cursor()
        cursor4 = self.get_connection().cursor()

        cursor1.execute(("SELECT * FROM administrateur WHERE email=?"),
                        (username,))
        cursor2.execute(("SELECT * FROM recruteur WHERE email=?"),
                        (username,))
        cursor3.execute(("SELECT * FROM consultant WHERE email=?"),
                        (username,))
        cursor4.execute(("SELECT * FROM contact WHERE email=?"),
                        (username,))

        admin = cursor1.fetchone()
        recruteur = cursor2.fetchone()
        consultant = cursor3.fetchone()
        client = cursor4.fetchone()

        if admin is not None:
            return (consultant, 0)
        elif recruteur is not None:
            return (recruteur, 1)
        elif client is not None:
            return (client, 2)
        elif consultant is not None:
            return (consultant, 3)
        else:
            return None

    def get_all_accounts_recruiters(self):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT * FROM recruteur")

        recruiters = cursor.fetchall()

        return [recruiteur_display(each) for each in recruiters]

    def get_all_accounts_consultants(self):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT * FROM consultant")

        consultants = cursor.fetchall()

        return [consultant_display(each) for each in consultants]

    def get_all_accounts_clients(self):
        cursor = self.get_connection().cursor()
        cursor.execute("SELECT * FROM contact")

        clients = cursor.fetchall()

        return [client_display(each) for each in clients]
    
    def reset_password(self, username, salt, hashed_password, utype):
        connection = self.get_connection()

        if utype == 1:
            connection.execute('UPDATE recruteur SET salt=?, hash=? WHERE email=?',
                              (salt, hashed_password, username))
        elif utype == 2:
            connection.execute('UPDATE contact SET salt=?, hash=? WHERE email=?',
                              (salt, hashed_password, username))
        else:
            connection.execute('UPDATE consultant SET salt=?, hash=? WHERE email=?',
                              (salt, hashed_password, username))

        connection.commit()
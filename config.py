# AMAConsulting/config.py

import os
import configparser

file_name = "config.ini"

if not os.path.isfile(file_name):
    config_file = open(file_name, 'w')

    config = configparser.ConfigParser()
    config.add_section('email')
    # EMAIL USERNAME CONFIGURATION
    config.set('email', 'USERNAME', 'coop.ama.consulting@gmail.com')
    # EMAIL PASSWORD CONFIGURATION
    config.set('email', 'PASSWORD', 'coopama2019')
    config.add_section('secret')
    config.set('secret', 'SECRET_KEY', 'coopama2019')
    config.set('secret', 'SECRET_PASSWORD_SALT', 'coopama2019')
    config.add_section('db')
    # DB PATH CONFIGURATION
    config.set('db', 'PATH', 'db/db.db')
    config.write(config_file)
    config_file.close()

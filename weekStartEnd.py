# AMAConsulting/weekStartEnd.py
from datetime import datetime, timedelta, date
from base import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .Soumission import Soumission
from .FeuilleDeTemps import FeuilleDeTemps
from .Ordre_achat import Ordre_achat
from .Avenant import Avenant
from .Client import Client
from .Notification import Notification
from .Facture import Facture
from .Abonnement import Abonnement

engine = create_engine('sqlite:///db/ama.db')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


ordre_achats = Ordre_achat.lister(session)
avenants = Avenant.lister(session)


def ajouter_feuille_oa():
    for ordre_achat in ordre_achats:
        client = (Soumission.afficher_id(ordre_achat.soumission_id, session)).client
        approbateur = (Client.afficher_gestionnaire(client, session)).email
        date_start = date(int(ordre_achat.date_debut[0:4]), int(ordre_achat.date_debut[5:7]), int(ordre_achat.date_debut[8:10]))
        date_end = date(int(ordre_achat.date_fin[0:4]), int(ordre_achat.date_fin[5:7]), int(ordre_achat.date_fin[8:10]))
        while date_start < date_end:
            start_of_week = date_start - timedelta(days=date_start.weekday())
            end_of_week = start_of_week + timedelta(days=6)
#            FeuilleDeTemps.ajouter(FeuilleDeTemps(ordre_achat_id = ordre_achat.id, date_debut = start_of_week, date_fin = end_of_week, approbateur = approbateur), session)
            if end_of_week < date(2019, 3, 4):
#                feuille = FeuilleDeTemps.afficher(ordre_achat.id, start_of_week, session)
#                feuille.jour_1 = "8:00 - 11:30 | 13:00 - 16:30"
#                feuille.jour_2 = "8:00 - 11:30 | 13:00 - 16:30"
#                feuille.jour_3 = "8:00 - 11:30 | 13:00 - 16:30"
#                feuille.jour_4 = "8:00 - 11:30 | 13:00 - 16:30"
#                feuille.jour_5 = "8:00 - 11:30 | 13:00 - 16:30"
#                feuille.etat = "approuvee"
                Notification.ajouter(Notification(ordre_achat_id=ordre_achat.id, emetteur="", receveur=approbateur,
                                                  date=start_of_week, contexte="Approbation feuille de temps",
                                                  contenu="Feuille de temps pour la semaine approuvee ",
                                                  etat="approuvee"), session)
            date_start = end_of_week + timedelta(days=1)


def ajouter_feuille_avenant():
    for avenant in avenants:
        ordre_achat = Ordre_achat.afficher_id(avenant.ordre_achat, session)
        client = (Soumission.afficher_id(ordre_achat.soumission_id, session)).client
        approbateur = (Client.afficher_gestionnaire(client, session)).email
        date_start = date(int(Ordre_achat.afficher_id(avenant.ordre_achat, session).date_fin[0:4]), int(Ordre_achat.afficher_id(avenant.ordre_achat, session).date_fin[5:7]), int(Ordre_achat.afficher_id(avenant.ordre_achat, session).date_fin[8:10])) + timedelta(days=6)
        date_end = date(int(avenant.date_fin[0:4]), int(avenant.date_fin[5:7]), int(avenant.date_fin[8:10]))
        while date_start < date_end:
            start_of_week = date_start - timedelta(days=date_start.weekday())
            end_of_week = start_of_week + timedelta(days=6)
#            FeuilleDeTemps.ajouter(FeuilleDeTemps(ordre_achat_id = avenant.ordre_achat, date_debut = start_of_week, date_fin = end_of_week, approbateur = approbateur), session)
            if end_of_week < date(2019, 3, 4):
#                feuille = FeuilleDeTemps.afficher(avenant.ordre_achat, start_of_week, session)
#                feuille.jour_1 = "8:00 - 11:30 | 13:00 - 16:30"
#                feuille.jour_2 = "8:00 - 11:30 | 13:00 - 16:30"
#                feuille.jour_3 = "8:00 - 11:30 | 13:00 - 16:30"
#                feuille.jour_4 = "8:00 - 11:30 | 13:00 - 16:30"
#                feuille.jour_5 = "8:00 - 11:30 | 13:00 - 16:30"
#                feuille.etat = "approuvee"
                Notification.ajouter(Notification(ordre_achat_id=ordre_achat.id, emetteur="", receveur=approbateur,
                                                  date=start_of_week, contexte="Approbation feuille de temps",
                                                  contenu="Feuille de temps pour la semaine approuvee ",
                                                  etat="approuvee"), session)
            date_start = end_of_week + timedelta(days=1)


#ajouter_feuille_oa()
#ajouter_feuille_avenant()
#feuille = FeuilleDeTemps.afficher(1,"2018-12-17" , session)
#feuille.jour_3 = 7
#print(feuille)
#feuille.etat = "soumise"

## ** Sprint 3 **##
# Le nombre de feuille de temps pour un mois particulier, entree OrdreAchat et une date
feuilleTempsMois = FeuilleDeTemps.lister_mois(3, "2019-01-07", session)
#print(feuilleTempsMois)

# Le nombre de feuille de temps pour du mois courant, entree OrdreAchat
today = datetime.today()
ce_mois = str(date(today.year, today.month, 1))
feuilleTempsMoisCourant = FeuilleDeTemps.mois_courant(5, ce_mois, session)
#print(feuilleTempsMoisCourant)

# Mensualite pour un tenant, entree le nom du tenant
mensualite = Abonnement.afficher_mensualite("AMA Consulting", session)
#print(mensualite[0])

# Le cout de la fauille de temps pour un tenant, entree le nom du tenant
coutFeuille = Abonnement.afficher_cout_feuille("AMA Consulting", session)
#print(coutFeuille[0])

# Le nombre de consultant max pour un tenant, entree le nom du tenant
consultantMax = Abonnement.afficher_max_consultant("AMA Consulting", session)
#print(consultantMax[0])

sousTotal = mensualite[0] + (feuilleTempsMois * coutFeuille[0])
#print(sousTotal)

# Facturer un tenant
facture = Facture(entreprise="AMA Consulting", date_facture=date.today(), mois=1 , annee=2019, mensualite=mensualite[0], nbr_feuille=feuilleTempsMois, cout_feuille=coutFeuille[0], sous_total=sousTotal)
Facture.ajouter(facture, session)

# Trouver les factures par tenant
Facture.trouver_tenant("AMA Consulting", session)

session.commit()

from sqlalchemy import Column, String, Integer, ForeignKey
from .base import Base
from .Ordre_achat import Ordre_achat


class Notification(Base):
    __tablename__ = 'notification'
    id = Column(Integer, primary_key=True)
    ordre_achat_id = Column(Integer, ForeignKey('ordre_achat.id'))
    emetteur = Column(String)
    receveur = Column(String)
    date = Column(String)
    contexte = Column(String)
    contenu = Column(String)
    etat = Column(String)

    def __repr__(self):
        return "<Notification(id=%d, ordre_achat_id=%d, emetteur=%s, receveur=%s, date=%s, contexte=%s, contenu=%s, etat=%s)>" \
               % (self.id, self.ordre_achat_id, self.emetteur, self.receveur, self.date, self.contexte, self.contenu, self.etat)

    def ajouter(notification, session):
        session.add(notification)
        session.commit()

    def afficher_notif_emetteur(emetteur, session):
        notifications = [instance for instance in session.query(
            Notification).filter(Notification.emetteur == emetteur)]
        return notifications
    
    def afficher_notif_receveur(receveur, session):
        notifications = [instance for instance in session.query(
            Notification).filter(Notification.receveur == receveur)]
        return notifications
    
    def afficher_notif_id(notif_id, session):
        for instance in session.query(Notification).filter(Notification.id == notif_id):
            return instance

    def afficher_notif_etat(user, notif_etat, session):
        for instance in session.query(Notification).filter(Notification.receveur == user).filter(Notification.etat == notif_etat):
            return instance

    def afficher_toute_notifs_non_lu(user, session):
        notifications = [instance for instance in session.query(Notification).filter(
            Notification.receveur == user).filter(Notification.etat == "non lu")]
        return notifications

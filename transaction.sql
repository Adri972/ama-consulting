--
-- File generated with SQLiteStudio v3.2.1 on Mon Apr 8 15:34:27 2019
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: transaction
CREATE TABLE [transaction] (
    id         TEXT    PRIMARY KEY,
    recruteur  TEXT    NOT NULL,
    date       TEXT    NOT NULL,
    id_facture INTEGER REFERENCES facture (id) 
                       NOT NULL,
    montant    INTEGER NOT NULL
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;

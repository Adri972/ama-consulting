--
-- File generated with SQLiteStudio v3.2.1 on Thu Mar 21 12:56:48 2019
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: notification
CREATE TABLE notification (
    id             INTEGER PRIMARY KEY,
    ordre_achat_id INTEGER REFERENCES ordre_achat (id),
    emetteur       TEXT,
    receveur       TEXT,
    date_debut     TEXT,
    contexte       TEXT,
    contenu        TEXT,
    etat           TEXT
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;

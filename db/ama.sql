CREATE TABLE IF NOT EXISTS tenant (
 entreprise		text    NOT NULL UNIQUE PRIMARY KEY,
 email 		    text    NOT NULL UNIQUE,
 telephone 		text
);

CREATE TABLE IF NOT EXISTS administrateur (
 id             integer PRIMARY KEY,
 email   		text    NOT NULL UNIQUE,
 FOREIGN KEY (email)    REFERENCES consultant (email)
);

CREATE TABLE IF NOT EXISTS recruteur (
 nom   		    text    NOT NULL,
 prenom         text    NOT NULL,
 email 		    text    NOT NULL NOT NULL UNIQUE PRIMARY KEY,
 telephone 		text	NOT NULL,
 entreprise     text	NOT NULL,
 salt           varchar, 
 hash           varchar,
 FOREIGN KEY (entreprise)  REFERENCES tenant (entreprise)
);

CREATE TABLE IF NOT EXISTS offre (
 type   		text    CHECK (type IN ('bronze', 'argent', 'or')) NOT NULL UNIQUE PRIMARY KEY,
 mensualite 	integer CHECK (mensualite IN ('10', '20', '35')),
 max_feuille 	integer CHECK (max_feuille IN ('1', '0.8', '0.7'))
 );

CREATE TABLE IF NOT EXISTS abonnement (
 entreprise		text    NOT NULL UNIQUE PRIMARY KEY,
 date_debut		date    NOT NULL,
 date_fin		date    NOT NULL, 
 offre          text	NOT NULL UNIQUE,
 FOREIGN KEY (entreprise)  REFERENCES tenant (entreprise),
 FOREIGN KEY (offre)    REFERENCES offre (type)
);	

CREATE TABLE IF NOT EXISTS client (
 nom_client		text	NOT NULL UNIQUE PRIMARY KEY,	
 entreprise	    text    NOT NULL,
 telephone 		text,	
 adresse 		text,
 FOREIGN KEY (entreprise)  REFERENCES tenant (entreprise)
);

CREATE TABLE IF NOT EXISTS contact (
 id 			integer PRIMARY KEY,
 nom 			text    NOT NULL,
 prenom 		text    NOT NULL,
 email 			text    NOT NULL UNIQUE,
 telephone 		text,
 fonction 		text check (fonction IN ('rh', 'gestionnaire')) NOT NULL,
 client 		text    NOT NULL,
 entreprise		text	NOT NULL,
 salt           varchar, 
 hash           varchar,
 FOREIGN KEY (entreprise)  REFERENCES tenant (entreprise),
 FOREIGN KEY (client) REFERENCES client (nom_client)
);

CREATE TABLE IF NOT EXISTS consultant (
 id 			integer PRIMARY KEY,
 nom 			text NOT NULL,    
 prenom 		text NOT NULL,
 email 			text NOT NULL UNIQUE,
 telephone 		text,
 adresse        text,
 ville          text,
 province       text,
 code_postal    text, 
 salt           varchar, 
 hash           varchar,
 disponible 	BOOLEAN NOT NULL, 
 entreprise		text	NOT NULL,
 FOREIGN KEY (entreprise)  REFERENCES tenant (entreprise)
 );	
 
CREATE TABLE IF NOT EXISTS demande (
 id 			integer PRIMARY KEY,
 date_demande   date NOT NULL,
 titre 			text NOT NULL,
 details 		text NOT NULL,
 duree 			text NULL,
 client 		text NOT NULL,
 contact        text NOT NULL,
 FOREIGN KEY (client) REFERENCES client (nom_client),
 FOREIGN KEY (contact) REFERENCES contact (email)
);

CREATE TABLE IF NOT EXISTS soumission (
 id 			integer PRIMARY KEY,
 date_soumission date NOT NULL,
 recruteur		text NOT NULL,
 client 		text NOT NULL,
 consultant 	text NOT NULL,
 demande_id 	integer NOT NULL,
 taux_horaire 	integer NOT NULL,
 duree 			text NOT NULL,
 details 		text NOT NULL,
 FOREIGN KEY (recruteur) REFERENCES recruteur (email),
 FOREIGN KEY (client) REFERENCES client (nom_client),
 FOREIGN KEY (consultant) REFERENCES consultant (email),
 FOREIGN KEY (demande_id) REFERENCES demande (id)
);

CREATE TABLE IF NOT EXISTS ordre_achat (
 id 			integer PRIMARY KEY,
 date_oa 		date    NOT NULL,
 date_debut		date    NOT NULL,
 date_fin		date    NOT NULL,
 soumission_id 	integer,
 FOREIGN KEY (soumission_id) REFERENCES soumission (id)
);

CREATE TABLE IF NOT EXISTS dotation(
 date_dotation	date 	NOT NULL,
 ordre_achat    integer PRIMARY KEY,
 ouvert			BOOLEAN ,
 FOREIGN KEY (ordre_achat) REFERENCES ordre_achat (id)	
);

CREATE TABLE IF NOT EXISTS avenant (
 date_avenant	date 	NOT NULL,
 date_fin		date 	NOT NULL,
 ordre_achat    integer PRIMARY KEY,
 contact        text 	NOT NULL,
 FOREIGN KEY (contact) REFERENCES contact (email),
 FOREIGN KEY (ordre_achat) REFERENCES ordre_achat (id) 
);


CREATE TABLE IF NOT EXISTS sessions (
 id             integer PRIMARY KEY,
 id_session     varchar,
 utilisateur    varchar
);

/* INSERT INTO administrateur(email) VALUES("adrien.lombard83@gmail.com");*/
/* USERS */
INSERT INTO consultant(nom,prenom,email,telephone,adresse,ville,province,code_postal,salt,hash,disponible,entreprise)
VALUES ("Lombard", "Adrien", "adrien.lombard@consultant.ca","(438) 926-7297","","","","","e6936781d06f489b910459fab6bb963c","5ebad724dde7d1cd8d1d12ee256994d31f69f291455672ceb96ce458f98aa2b4da9574c764bf38d242281322367c0aa08713bb869b3c336a488354889f328ce4",1,"AMA Consulting");

INSERT INTO recruteur(nom,prenom,email,telephone,entreprise,salt,hash)
VALUES ("Lombard", "Adrien", "adrien.lombard@recruteur.ca","(438) 926-7297","AMA Consulting","e6936781d06f489b910459fab6bb963c","5ebad724dde7d1cd8d1d12ee256994d31f69f291455672ceb96ce458f98aa2b4da9574c764bf38d242281322367c0aa08713bb869b3c336a488354889f328ce4");

INSERT INTO contact(nom,prenom,email,telephone,fonction,client,entreprise,salt,hash)
VALUES ("Lombard", "Adrien", "adrien.lombard@contact.ca","(438) 926-7297","rh","Ville de Montreal","AMA Consulting","e6936781d06f489b910459fab6bb963c","5ebad724dde7d1cd8d1d12ee256994d31f69f291455672ceb96ce458f98aa2b4da9574c764bf38d242281322367c0aa08713bb869b3c336a488354889f328ce4");

/* DEMANDE */
INSERT INTO demande(date_demande,titre,details,date_d,duree,client,contact)
VALUES ("2019-03-09","Analyste Developpeur","Developpeur web","2019-03-09","6 mois","Ville de Montreal","adrien.lombard@contact.ca");

/* SOUMISSION*/
INSERT INTO soumission(date_soumission,recruteur,client,consultant,demande_id,taux_horaire,duree,details)
VALUES ("2019-03-09","adrien.lombard@recruteur.ca","Ville de Montreal","adrien.lombard@consultant.ca","7","100","6 mois","Renouvelable");

/* ORDRE D'ACHAT */
INSERT INTO ordre_achat(date_oa,date_debut,date_fin,soumission_id)
VALUES ("2019-03-09","2019-03-10","2019-03-31","7");

/* DOTATION */
INSERT INTO dotation(date_dotation,ordre_achat,ouvert)
VALUES ("2019-03-09","7",1);

/* AVENANT */
INSERT INTO avenant(date_avenant,date_fin,ordre_achat,contact)
VALUES ("2019-03-09","2019-03-31","7","adrien.lombard@contact.ca"); 

/* ALI */
INSERT INTO consultant(nom,prenom,email,telephone,adresse,ville,province,code_postal,salt,hash,disponible,entreprise)
VALUES ("Sultani", "Ali", "ali.sultani@amaconsulting.ca","(514) 000-0000","","","","","0a7b511408ee443c853c50b919ccfb4f","7e7f2b7b6a227bc078614a1bcfc4ef15aa1cf36717d45f1b9fe5f18ed206c98c362cc9709a81917011ce37da1214d9559fd7fd7482c163c298516177fd38c964",1,"AMA Consulting");

INSERT INTO `demande` (id,date_demande,titre,details,date_d,duree,client,contact) VALUES (8,'2019-03-09','Analyste Developpeur','Developpeur web','2019-03-09','6 mois','Ville de Laval','agirard@laval.ca');

INSERT INTO `soumission` (id,date_soumission,recruteur,client,consultant,demande_id,taux_horaire,duree,details) VALUES (8,'2019-03-09','recruteur2@amaconsulting.ca','Ville de Laval','ali.sultani@amaconsulting.ca',8,100,'6 mois','Renouvelable');

INSERT INTO `ordre_achat` (id,date_oa,date_debut,date_fin,soumission_id) VALUES (8,'2019-03-09','2019-03-10','2019-03-31',8);

/* DOTATION */
INSERT INTO dotation(date_dotation,ordre_achat,ouvert)
VALUES ("2019-03-09",8,1);

/* AVENANT */
INSERT INTO avenant(date_avenant,date_fin,ordre_achat,contact)
VALUES ("2019-03-09","2019-03-31",8,"agirard@laval.ca"); 




/* RECRUTEUR 2 */

INSERT INTO recruteur(nom,prenom,email,telephone,entreprise,salt,hash)
VALUES ("Jean", "Dupont", "recruteur2@amaconsulting.ca","(438) 926-7297","AMA Consulting","e6936781d06f489b910459fab6bb963c","5ebad724dde7d1cd8d1d12ee256994d31f69f291455672ceb96ce458f98aa2b4da9574c764bf38d242281322367c0aa08713bb869b3c336a488354889f328ce4");


/* DEMANDE */
INSERT INTO demande(date_demande,titre,details,date_d,duree,client,contact)
VALUES ("2019-03-09","Analyste Developpeur","Developpeur web","2019-03-09","6 mois","Ville de Laval","agirard@laval.ca");




/* DOTATION */
INSERT INTO dotation(date_dotation,ordre_achat,ouvert)
VALUES ("2019-03-09","8",1);

/* AVENANT */
INSERT INTO avenant(date_avenant,date_fin,ordre_achat,contact)
VALUES ("2019-03-09","2019-03-31","8","agirard@laval.ca"); 
create table users (
    id integer primary key,
    utype integer(1),
    nom varchar(25),
    prenom varchar(25),
    email varchar(100),
    tel varchar(10),
    adresse varchar(100),
    ville varchar(25),
    province varchar(50),
    code_postal varchar(7),
    salt varchar(32),
    hash varchar(128),
    confirmed integer(1)
);

create table sessions (
    id integer primary key,
    id_session varchar(32),
    utilisateur varchar(25)
);
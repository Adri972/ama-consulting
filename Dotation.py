# AMAConsulting/Dotation.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from .base import Base

class Dotation(Base):
    __tablename__ = 'dotation'
    date_dotation = Column(String)
    ordre_achat = Column(Integer, ForeignKey('ordre_achat.id'), primary_key=True)
    ouvert = Column(Boolean)

    def __repr__(self):
        return "<Dotation(date_dotation='%s', ordre_achat='%d', ouvert='%d'>" \
        % (self.date_dotation, self.ordre_achat, self.ouvert)

    @staticmethod
    def afficher_dotations(session):
        for instance in session.query(Dotation):
            print(instance)

    def ajouter_multi(dotations, session):
        for dotation in dotations:
            session.add(dotation)

    def ajouter(dotation, session):
        session.add(dotation)
        session.commit()

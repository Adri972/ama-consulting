# AMAConsulting/test.py

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select
import base
from Offre import *
from Tenant import *
from Recruteur import *
from Abonnement import *
from Client import *
from Contact import *
from Consultant import *
from Demande import *
from Soumission import *
from Ordre_achat import *
from Dotation import *
from Avenant import *

engine = create_engine('sqlite:///db/ama.db')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


offres = [Offre(type='or', mensualite='35', max_feuille='0.7'),
          Offre(type='argent', mensualite='20', max_feuille='0.8'),
          Offre(type='bronze', mensualite='10', max_feuille='1')]
tenants = [Tenant(entreprise='AMA Consulting', email='contact@amaconsulting.ca',telephone='5143216789'),
           Tenant(entreprise='Coop TI', email='Coop TI', telephone='5141111234')]
recruteurs = [Recruteur(nom='Lombard', prenom='Adrien', email='alombard@amaconsulting.ca', telephone='5143216789', entreprise='AMA Consulting', salt='Secret123', hash='Secret123'),
              Recruteur(nom='Lehoux', prenom='Alain', email='alehoux@coopti.ca', telephone='5141111235', entreprise='Coop TI', salt='Secret123', hash='Secret123',)]
abonnements = [Abonnement(entreprise='AMA Consulting', date_debut='2018-01-01', date_fin='2038-12-31', offre='or'),
               Abonnement(entreprise='Coop TI', date_debut='2019-01-01', date_fin='2019-12-31', offre='argent')]
clients = [Client(nom_client='Ville de Montreal',entreprise='AMA Consulting', telephone='5145547818', adresse=''),
           Client(nom_client='Ville de Laval',entreprise='AMA Consulting', telephone='5144456542', adresse=''),
           Client(nom_client='Ville de Longueuil',entreprise='AMA Consulting', telephone='5141575123', adresse=''),
           Client(nom_client='Ville de Quebec',entreprise='Coop TI', telephone='5141118236', adresse=''),
           Client(nom_client='Ville de Sherbrooke',entreprise='Coop TI', telephone='5141115854', adresse=''),
           Client(nom_client='Ville de Levis',entreprise='Coop TI', telephone='5141112389', adresse='')]
contacts = [Contact(nom='Dubois', prenom='Julie', email='jdubois@montreal.ca', telephone='5145556666', fonction='rh', client='Ville de Montreal', entreprise='AMA Consulting',salt='Secret123', hash='Secret123'),
            Contact(nom='Lamarche',prenom='Marc',email='mlamarche@montreal.ca',telephone='5145556667',fonction='rh',client='Ville de Montreal',entreprise='AMA Consulting',salt='Secret123', hash='Secret123'),
            Contact(nom='Leger',prenom='Alain',email='aleger@montreal.ca',telephone='5145556668',fonction='gestionnaire',client='Ville de Montreal',entreprise='AMA Consulting',salt='Secret123', hash='Secret123'),
            Contact(nom='Girard',prenom='Amelie',email='agirard@laval.ca',telephone='4505556666',fonction='rh',client='Ville de Laval',entreprise='AMA Consulting',salt='Secret123', hash='Secret123'),
            Contact(nom='Breton',prenom='France',email='fbreton@laval.ca',telephone='4505556667',fonction='rh',client='Ville de Laval',entreprise='AMA Consulting',salt='Secret123', hash='Secret123'),
            Contact(nom='Lebel',prenom='Mario',email='mlabel@laval.ca',telephone='4505556668',fonction='gestionnaire',client='Ville de Laval',entreprise='AMA Consulting',salt='Secret123', hash='Secret123'),
            Contact(nom='Simard',prenom='Catherine',email='csimard@longueuil.ca',telephone='4504446666',fonction='rh',client='Ville de Longueuil',entreprise='AMA Consulting',salt='Secret123', hash='Secret123'),
            Contact(nom='Gauthier',prenom='Marc',email='mgauthier@longueuil.ca',telephone='4504446667',fonction='rh',client='Ville de Longueuil',entreprise='AMA Consulting',salt='Secret123', hash='Secret123'),
            Contact(nom='Noel',prenom='Melanie',email='mnoel@longueuil.ca',telephone='4504446668',fonction='gestionnaire',client='Ville de Longueuil',entreprise='AMA Consulting',salt='Secret123', hash='Secret123'),
            Contact(nom='Roy', prenom='Amelie', email='aroy@quebec.ca', telephone='5141112547', fonction='rh', client='Ville de Quebec', entreprise='Coop TI', salt='Secret123', hash='Secret123'),
            Contact(nom='Dumont',prenom='Olivier', email='odumont@quebec.ca', telephone='5141112546', fonction='rh', client='Ville de Quebec', entreprise='Coop TI', salt='Secret123', hash='Secret123'),
            Contact(nom='Legault',prenom='Francis', email='flegault@quebec.ca', telephone='5141112545', fonction='gestionnaire', client='Ville de Quebec', entreprise='Coop TI', salt='Secret123', hash='Secret123'),
            Contact(nom='Lemard',prenom='Stephanie', email='slamard@sherbrooke.ca', telephone='4502221258', fonction='rh', client='Ville de Sherbrooke', entreprise='Coop TI', salt='Secret123', hash='Secret123'),
            Contact(nom='Martin',prenom='Rosalie', email='rmartin@sherbrooke.ca', telephone='4502221257', fonction='rh', client='Ville de Sherbrooke', entreprise='Coop TI', salt='Secret123', hash='Secret123'),
            Contact(nom='Simard',prenom='Melanie', email='msimard@sherbrooke.ca', telephone='4502222156', fonction='gestionnaire', client='Ville de Sherbrooke', entreprise='Coop TI', salt='Secret123', hash='Secret123'),
            Contact(nom='Maltais', prenom='Cathy', email='cmaltais@levis.ca', telephone='450333627', fonction='rh', client='Ville de Levis', entreprise='Coop TI', salt='Secret123', hash='Secret123'),
            Contact(nom='Maltais', prenom='Catia', email='clamote@levis.ca', telephone='4503333626', fonction='rh', client='Ville de Levis', entreprise='Coop TI', salt='Secret123', hash='Secret123'),
            Contact(nom='Smith', prenom='Ashley', email='asmith@levis.ca', telephone='4503333625', fonction='gestionnaire', client='Ville de Levis', entreprise='Coop TI', salt='Secret123', hash='Secret123')]

consultants = [Consultant(nom='Rbiji', prenom='Mohammed', email='mohammed.rbiji@ama.ca', telephone='5145764223', adresse='', ville='', province='', code_postal='', salt='Secret123', hash='Secret123', disponible=False, entreprise='AMA Consulting'),
               Consultant(prenom='Adrien', nom='Lombard', email='adrien.lombard@ama.ca', telephone='5143664587', adresse='', ville='', province='', code_postal='', salt='Secret123', hash='Secret123', disponible=False, entreprise='AMA Consulting'),
               Consultant(prenom='Ali', nom='Sultani', email='ali.sultani@ama.ca', telephone='5142315462', adresse='', ville='', province='', code_postal='', salt='Secret123', hash='Secret123', disponible=False, entreprise='AMA Consulting'),
               Consultant(prenom='Carl', nom='Boucher', email='carl.boucher@coopti.ca', telephone='5141112388', adresse='', ville='', province='', code_postal='', salt='Secret123', hash='Secret123', disponible=False, entreprise='Coop TI'),
               Consultant(prenom='Dany', nom='Fillion', email='dany.fillion@coopti.ca', telephone='5141112387', adresse='', ville='', province='', code_postal='', salt='Secret123', hash='Secret123', disponible=False, entreprise='Coop TI'),
               Consultant(prenom='David', nom='Morin', email='david.morin@coopti.ca', telephone='5141112386', adresse='', ville='', province='', code_postal='', salt='Secret123', hash='Secret123', disponible=False, entreprise='Coop TI')]

demandes = [Demande(date_demande='2018-01-15', titre='Architecte Logiciel', details='Architect Logiciel C++/JAVA', date_d='2019-04-01' ,duree='6 mois' ,client='Ville de Montreal' ,contact='jdubois@montreal.ca'),
            Demande(date_demande='2018-01-10', titre='Analyste Developpeur', details='Developpeur web', date_d='2019-03-15' ,duree='12 mois' ,client='Ville de Laval' ,contact='fbreton@laval.ca'),
            Demande(date_demande='2018-01-23', titre='Analyste BI', details='Analyste daffaire BI', date_d='2019-04-18' ,duree='6 mois' ,client='Ville de Longueuil' ,contact='mgauthier@longueuil.ca'),
            Demande(date_demande='2019-02-01', titre='Architecte Entreprise', details='Architect Entreprise TOGAF', date_d='2019-02-15' ,duree='6 mois' ,client='Ville de Quebec' ,contact='odumont@quebec.ca'),
            Demande(date_demande='2019-02-05', titre='Architecte de donnees', details='Architecte de donnees Oracle', date_d='2019-02-20' ,duree='12 mois' ,client='Ville de Sherbrooke' ,contact='rmartin@sherbrooke.ca'),
            Demande(date_demande='2019-02-10', titre='Analyste Affaire', details='Analyste affaire BI', date_d='2019-02-25' ,duree='6 mois' ,client='Ville de Levis' ,contact='cmaltais@levis.ca')]

soumissions = [Soumission(date_soumission='2018-01-28', recruteur='alombard@amaconsulting.ca', client='Ville de Montreal', consultant='mohammed.rbiji@ama.ca', demande_id='1', taux_horaire='100', duree='6 mois',details='Rebouvelable'),
               Soumission(date_soumission='2018-01-21', recruteur='alombard@amaconsulting.ca', client='Ville de Laval', consultant='ali.sultani@ama.ca', demande_id='2', taux_horaire='100', duree='6 mois',details='Rebouvelable'),
               Soumission(date_soumission='2018-02-02', recruteur='alombard@amaconsulting.ca', client='Ville de Longueuil', consultant='adrien.lombard@ama.ca', demande_id='3', taux_horaire='100', duree='6 mois',details='Rebouvelable'),
               Soumission(date_soumission='2019-02-04', recruteur='alehoux@coopti.ca', client='Ville de Quebec', consultant='carl.boucher@coopti.ca', demande_id='4', taux_horaire='100', duree='6 mois',details='Rebouvelable'),
               Soumission(date_soumission='2019-02-09', recruteur='alehoux@coopti.ca', client='Ville de Sherbrooke', consultant='dany.fillion@coopti.ca', demande_id='5', taux_horaire='100', duree='6 mois',details='Rebouvelable'),
               Soumission(date_soumission='2019-02-14', recruteur='alehoux@coopti.ca', client='Ville de Levis', consultant='david.morin@coopti.ca', demande_id='6', taux_horaire='100', duree='6 mois',details='Rebouvelable')]

ordre_achats = [Ordre_achat(date_oa='2018-02-10', soumission_id='1', date_debut='2018-03-15', date_fin='2018-09-14'),
                Ordre_achat(date_oa='2018-02-18', soumission_id='2', date_debut='2018-03-01', date_fin='2019-02-28'),
                Ordre_achat(date_oa='2018-02-24', soumission_id='3', date_debut='2018-03-10', date_fin='2018-09-09'),
                Ordre_achat(date_oa='2019-02-10', soumission_id='4', date_debut='2019-02-15', date_fin='2019-08-09'),
                Ordre_achat(date_oa='2019-02-15', soumission_id='5', date_debut='2019-02-20', date_fin='2019-08-14'),
                Ordre_achat(date_oa='2019-02-20', soumission_id='6', date_debut='2019-02-25', date_fin='2019-08-19')]

dotations = [Dotation(date_dotation='2018-02-15', ordre_achat='1', ouvert=True),
             Dotation(date_dotation='2018-02-16', ordre_achat='2', ouvert=True),
             Dotation(date_dotation='2018-02-14', ordre_achat='3', ouvert=True),
             Dotation(date_dotation='2019-02-12', ordre_achat='4', ouvert=True),
             Dotation(date_dotation='2019-02-16', ordre_achat='5', ouvert=True),
             Dotation(date_dotation='2019-02-18', ordre_achat='6', ouvert=True)]

avenants = [Avenant(date_avenant='2018-08-10', date_fin='2019-03-14', ordre_achat='1', contact='mlamarche@montreal.ca'),
            Avenant(date_avenant='2018-08-10', date_fin='2019-03-09', ordre_achat='3', contact='mgauthier@longueuil.ca')]


Offre.ajouter(offres, session)
Tenant.ajouter(tenants, session)
Recruteur.ajouter(recruteurs, session)
Abonnement.ajouter(abonnements, session)
Client.ajouter(clients, session)
Contact.ajouter(contacts, session)
Consultant.ajouter(consultants, session)
Demande.ajouter(demandes, session)
Soumission.ajouter(soumissions, session)
Ordre_achat.ajouter(ordre_achats, session)
Dotation.ajouter(dotations, session)
Avenant.ajouter(avenants, session)

#Contact.afficher_par_email("msimard@sherbrooke.ca", session)
#Contact.afficher_par_tenant("Coop TI", session)
#Contact.afficher_par_telephone("4503333625", session)
#Contact.afficher_soumission_contact("mgauthier@longueuil.ca", session)
#Contact.ajouter([Contact(prenom='Justin', nom='Sirois', email='jsirois.ca', telephone='5145556600', fonction='rh', client='Ville de Montreal', entreprise='AMA Consulting',hash='Secret123')], session)
#Tenant.afficher_tenants(session)
#Tenant.afficher_recruteur("Coop TI", session)
#Tenant.afficher_client("Coop TI", session) # Completed
#Tenant.afficher_cconsultant("Coop TI", session)
#Tenant.afficher_demande("AMA Consulting", session)
#Tenant.afficher_soumission("Coop TI", session)
#Tenant.ajouter([Tenant(entreprise='High Tech', email='contact@hightech.ca',telephone='5143126879')], session)
#Client.afficher_contact("Ville de Levis", session)
#Client.afficher_demande("Ville de Montreal", session)
#Client.afficher_soumission("Ville de Montreal", session)
#Client.afficher_oa("Ville de Levis", session)
#Client.afficher_dotation("Ville de Longueuil", session)
#Client.afficher_dotation("Ville de Longueuil", session)
#Client.afficher_avenant("Ville de Montreal", session)
#Client.afficher_tenant("Ville de Laval", session)
#Consultant.afficher_soumission("ali.sultani@ama.ca", session)
#Consultant.afficher_oa("ali.sultani@ama.ca", session)
#Consultant.afficher_dotation("ali.sultani@ama.ca", session)
#Consultant.afficher_avenant("adrien.lombard@ama.ca", session)
#Demande.afficher_client(6, session)
#Demande.afficher_contact(4, session)
#Demande.afficher_soumission(5, session)
#Demande.afficher_oa(6, session)
#Soumission.afficher_client(1, session)
#Soumission.afficher_consultant(1, session)


session.commit()



# AMAConsulting/Demande.py
# -*- coding: UTF-8 -*-
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from .base import Base
from .Ordre_achat import Ordre_achat
from .Soumission import Soumission


class Demande(Base):
	__tablename__ = 'demande'
	id = Column(Integer, primary_key=True)
	date_demande = Column(String)
	titre = Column(String)
	details = Column(String)
	duree = Column(String)
	client = Column(String, ForeignKey('client.nom_client'))
	contact = Column(String, ForeignKey('contact.email'))

	def __repr__(self):
		return "<Demande( date_demande='%s', titre='%s', details='%s', date_d='%s', duree='%s', client='%s', contact='%s')>" \
			   % (self.date_demande, self.titre, self.details, self.date_demande, self.duree, self.client, self.contact )

	@staticmethod
	def afficher_client(demande, session):
		for instance in session.query(Demande.client).filter(Demande.id == demande):
			print(instance)

	@staticmethod
	def afficher_contact(demande, session):
		for instance in session.query(Demande.contact).filter(Demande.id == demande):
			print(instance)

	@staticmethod
	def afficher_soumission(demande, session):
		for instance in session.query(Soumission).filter(
				Soumission.demande_id.is_(
					session.query(Soumission.demande_id).filter(Soumission.demande_id == demande))):
			print(instance)

	@staticmethod
	def afficher_oa(demande, session):
		for instance in session.query(Ordre_achat).filter(
				Ordre_achat.soumission_id.is_(session.query(Soumission.demande_id).filter(Soumission.demande_id == demande))):
			print(instance)

	def ajouter(demandes, session):
		for demande in demandes:
			session.add(demande)


--
-- File generated with SQLiteStudio v3.2.1 on Mon Mar 18 13:55:06 2019
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: feuille_temps
CREATE TABLE feuille_temps (
    id             INTEGER PRIMARY KEY,
    date_debut     DATE,
    date_fin       DATE,
    jour_1         TEXT,
    jour_2         TEXT,
    jour_3         TEXT,
    jour_4         TEXT,
    jour_5         TEXT,
    jour_6         TEXT,
    jour_7         TEXT,
    approbateur    TEXT,
    etat           TEXT    CHECK (etat IN ('attente', 'soumise', 'approuvee', 'rejetee') ) 
                           DEFAULT 'attente',
    ordre_achat_id INTEGER REFERENCES ordre_achat (id),
    UNIQUE (
        ordre_achat_id,
        date_debut
    )
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
